# [Torpedo Alley](https://gitlab.com/adeharo9/torpedo-alley)

A submarine shoot-and-dodge game, based on [1982's Seafox](https://en.wikipedia.org/wiki/Seafox_(video_game)).

> ℹ️ This is a work in progress.

## Motivation

I used to play this game a lot when I was a kid on a [website my dad coded](https://web.archive.org/web/20040426235032/http://www.amordedios.org/juegos/torpedo/index.html),
so I decided to bring it back to life by refactoring it a bit and modernizing the technologies it uses so it can still
be played in 2023.

### What this is

- A personal project I do for fun.
- A way of bringing an old game I liked to life.
- A way of using some of my spare time for coding outside (my already coding-related) work.
- A way of sanitizing my relationship with coding and programming outside of work.
  - Yes, I love coding.
  - Yes, I am tired of coding after 8 straight hours of work.
  - Yes, work steals all the energies I have for coding.
  - No, I haven't coded after work for ages because of the stated reasons.
  - No, I haven't coded on weekends for ages because of the stated reasons.
  - Yes, this is my own attempt at not allowing work to monopolize my abilities for coding.

### What this is not

- A business project.
- A project that follows deadlines.
- An intent at doing the best possible implementation of "Torpedo Alley".
- An intent of using the best and most up-to-date technologies.
- Some sort of portfolio-building project.
- Something related to the concepts "productivity", "efficiency" or "profit".

## Roadmap

The following is an approximation at the tasks that I anticipate will have to be done before the project is finished and
"Torpedo Alley" can be played again in any modern platform:

- [x] Retrieve the original bytecode
- [x] Decompile bytecode into Java code
- [ ] Refactor Java code to make it more understandable: update variable names, un-substitute inline variables, etc.
  - [ ] CapitalShip
  - [ ] DepthCharge
  - [ ] Destroyer
  - [ ] Dolphin
  - [ ] EnemySub
  - [ ] EnemyTorpedo
  - [ ] HorizontalTorpedo
  - [ ] HospitalShip
  - [ ] KillerFish
  - [ ] MagneticMine
  - [ ] Package
  - [ ] Player
  - [ ] Seafox
  - [ ] SupplySub
  - [ ] VerticalTorpedo
- [ ] Comment Java code line by line so the purpose of every line of code is clear
  - [x] CapitalShip
  - [ ] DepthCharge
  - [ ] Destroyer
  - [ ] Dolphin
  - [ ] EnemySub
  - [ ] EnemyTorpedo
  - [ ] HorizontalTorpedo
  - [ ] HospitalShip
  - [ ] KillerFish
  - [ ] MagneticMine
  - [ ] Package
  - [ ] Player
  - [ ] Seafox
  - [ ] SupplySub
  - [ ] VerticalTorpedo
- [ ] Decouple existing code from `java.awt` by introducing an interface and a separate implementation.
- [ ] Decouple existing code from `java.applet` by introducing an interface and a separate implementation.
- [ ] Find a suitable replacement framework for the GUI:
  - Swing
  - JavaFX
  - Compose multiplatform
  - React
- [ ] Migrate the GUI
- [ ] Migrate Java code to Kotlin
- [ ] Improve code structure by decoupling classes to the extent possible
- [ ] Host the game
