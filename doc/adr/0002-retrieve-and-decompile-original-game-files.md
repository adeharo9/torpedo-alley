# 2. Retrieve and decompile original game files

Date: 2023-11-03

## Status

Accepted

## Context

The source code of the original Torpedo Alley game is not available on the Internet.

The game was originally served via Java Applets, which relied on sending the compiled bytecode to the target machine in
order to be executed.

A copy of the original game is still being served in [Richy's Organ Page](https://richmcveigh.tripod.com/indexalley.html).

Even if modern browsers are no longer able to render the game, a quick study of the deprecated [`<applet>` HTML tag](https://docs.oracle.com/javase/8/docs/technotes/guides/jweb/applet/using_tags.html)
revealed a way of manually retrieving the `.class` files hosted on the server (the `code` parameter indicates the main
file location relative to the current document's URL), which can be used to generate the equivalent source code to the
original code of the game.

Both `.class` files and the rest of resource files (GIFs and audio) can be retrieved from the [base URL of the hosting
server](https://richmcveigh.tripod.com/), appending the file name to it. E.g. `https://richmcveigh.tripod.com/Seafox.class`.

## Decision

The compiled bytecode files will be decompiled using [CFR 0.150](http://www.javadecompilers.com/) in order to generate
source code that can be used to rebuild the game.

Class file names will be retrieved by decompiling `Seafox.class` (the main class of the game being loaded in the applet)
and inspecting any missing classes during the decompiling process. This process will be repeated after downloading the
corresponding `.class` files until no classes are missing.

Resource file names will be retrieved after decompiling all `.class` files by manually inspecting the code for `.gif`
and `.au` file extension usages on file loading methods.

## Consequences

The generated code will be equivalent to the original code, but less understandable, as information is lost during the
compilation/decompilation process: variable names, static value substitution, inline methods, etc.

All resource files will be available.
