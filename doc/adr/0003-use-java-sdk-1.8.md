# 3. Use Java SDK 1.8

Date: 2023-11-03

## Status

Accepted

## Context

The decompiled source code makes use of the `java.applet` and `java.awt` API, which have been deprecated for almost over
a decade now, since Java 9.

## Decision

The Java SDK 1.8 (targeting Java 8) will be used to provide compatibility with the deprecated APIs in order to gain
further understanding of how the original code worked.

The implementation used will be corretto 1.8.0_392, as it's readily available to download in IntelliJ.

This is only meant to be a temporary measure to study the existing code, until a new, modern version is generated.

## Consequences

The deprecated `java.applet` and `java.awt` APIs will be able to be used.

The generated code will be using an outdated ([although supported](https://en.wikipedia.org/wiki/Java_version_history#Release_table))
version of Java.
