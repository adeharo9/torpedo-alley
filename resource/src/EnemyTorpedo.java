/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  Player
 *  VerticalTorpedo
 */
import java.awt.Graphics;
import java.awt.Image;

class EnemyTorpedo {
    public static Image m_imageTorpedo;
    public static double m_dTorpedoWidth;
    public static double m_dTorpedoHeight;
    public static final int WAKE_IMAGES = 4;
    public static Image[] m_imageWake;
    public static double m_dWakeWidth;
    public static final int MAX_TORPEDOES = 3;
    public static int m_nTorpedoLimit;
    public static int m_nActiveTorpedoes;
    static final int MIN_FIRE_INTERVAL = 12000;
    static final int MAX_FIRE_INTERVAL = 18000;
    static final double LAUNCH_X = 50.0;
    static final double LAUNCH_Y = 14.0;
    double m_dX;
    double m_dY;
    double m_dXstart;
    double m_dYstart;
    double m_dXvelocity;
    double m_dYvelocity;
    int m_nX;
    int m_nY;
    int m_nBlastX;
    int m_nBlastY;
    public int m_nStatus;
    public static final int INACTIVE = 0;
    static final int NORMAL = 1;
    public static final int EXPLODING = 2;
    long m_lTimeStatusUpdated;

    public void Fire(EnemySub enemySub) {
        this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
        this.m_dXstart = enemySub.m_dX + 50.0 + m_dTorpedoWidth;
        this.m_dYstart = enemySub.m_dY + 14.0 + m_dTorpedoHeight;
        this.m_dX = this.m_dXstart;
        this.m_dY = this.m_dYstart;
        this.m_dXvelocity = enemySub.m_dVelocity + 15.0;
        this.m_dYvelocity = 0.0;
        if (Player.m_dY < enemySub.m_dY) {
            this.m_dYvelocity = -5.0;
        }
        if (Player.m_dY > enemySub.m_dY) {
            this.m_dYvelocity = 5.0;
        }
        this.m_nStatus = 1;
        ++m_nActiveTorpedoes;
    }

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 1) {
            this.m_nX = (int)(this.m_dX - m_dTorpedoWidth);
            this.m_nY = (int)(this.m_dY - m_dTorpedoHeight / 2.0);
            graphics.drawImage(m_imageTorpedo, this.m_nX, this.m_nY, null);
            int n = (int)(this.m_dX - m_dTorpedoWidth - m_dWakeWidth);
            int n2 = this.m_nY + (int)m_dTorpedoHeight / 2 + 1;
            graphics.drawImage(m_imageWake[(Seafox.m_random.nextInt() & 0x7FFFFFF) % 4], n, n2, null);
            return;
        }
        if (this.m_nStatus == 2) {
            int n = (Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4;
            graphics.drawImage(VerticalTorpedo.m_imageBlast[n], this.m_nBlastX, this.m_nBlastY, null);
            return;
        }
    }

    static {
        m_imageWake = new Image[4];
    }

    EnemyTorpedo() {
    }

    public void Update() {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            double d = (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
            this.m_dX = this.m_dXstart + this.m_dXvelocity * d;
            this.m_dY = this.m_dYstart + this.m_dYvelocity * d;
            if (this.m_dX - m_dTorpedoWidth - m_dWakeWidth > (double)Seafox.m_nPlayfieldWidth) {
                this.m_nStatus = 0;
                m_nActiveTorpedoes += -1;
                return;
            }
            if (this.m_dY < Player.m_dYmin) {
                this.m_dY = Player.m_dYmin;
            }
            if (Seafox.m_player.CollisionAnalysis(this.m_dX, this.m_dY) || Seafox.m_supplySub.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nStatus = 0;
                m_nActiveTorpedoes += -1;
                return;
            }
            if (Seafox.m_dolphin.CollisionAnalysis(this.m_dX, this.m_dY) || Seafox.m_package.CollisionAnalysis(this.m_dX, this.m_dY) || Seafox.m_verticalTorpedo.CollisionAnalysis(this.m_dX - m_dTorpedoWidth, this.m_dY - m_dTorpedoHeight / 2.0, this.m_dX, this.m_dY + m_dTorpedoHeight / 2.0)) {
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            }
            double d2 = this.m_dX - m_dTorpedoWidth;
            double d3 = this.m_dY - m_dTorpedoHeight / 2.0;
            double d4 = this.m_dY + m_dTorpedoHeight / 2.0;
            int n = 0;
            do {
                if (!Seafox.m_depthCharge[n].CollisionAnalysis(d2, d3, this.m_dX, d4)) continue;
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
            n = 0;
            do {
                if (!Seafox.m_magneticMine[n].CollisionAnalysis(d2, d3, this.m_dX, d4)) continue;
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
        }
        if (this.m_nStatus == 2 && (int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) >= 500) {
            this.m_nStatus = 0;
            m_nActiveTorpedoes += -1;
            return;
        }
    }
}
