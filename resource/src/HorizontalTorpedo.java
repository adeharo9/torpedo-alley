/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  Player
 *  VerticalTorpedo
 */
import java.awt.Graphics;
import java.awt.Image;

class HorizontalTorpedo {
    public static Image m_imageTorpedo;
    public static double m_dTorpedoWidth;
    public static double m_dTorpedoHeight;
    static final double X_FIRE_OFFSET = 50.0;
    static final double Y_FIRE_OFFSET = 14.0;
    static final double X_VELOCITY = 35.0;
    static final double Y_VELOCITY = 8.0;
    double m_dXstart;
    double m_dYstart;
    double m_dXvelocity;
    double m_dYvelocity;
    int m_nBlastX;
    int m_nBlastY;
    double m_dX;
    double m_dY;
    int m_nX;
    int m_nY;
    public static final int INACTIVE = 0;
    static final int NORMAL = 1;
    public static final int EXPLODING = 2;
    public int m_nStatus;
    long m_lTimeStatusUpdated;

    public void Fire(Player player) {
        this.m_dXstart = Player.m_dX + 50.0 + m_dTorpedoWidth;
        this.m_dYstart = Player.m_dY + 14.0;
        this.m_dX = this.m_dXstart;
        this.m_dY = this.m_dYstart;
        this.m_dXvelocity = 35.0;
        if (Player.m_dYvelocity > 0.0) {
            this.m_dYvelocity = 8.0;
        }
        if (Player.m_dYvelocity < 0.0) {
            this.m_dYvelocity = -8.0;
        }
        if (Player.m_dYvelocity == 0.0) {
            this.m_dYvelocity = 0.0;
        }
        this.m_nStatus = 1;
        this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
        Seafox.m_bPlayFireTorpedo = true;
    }

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            this.m_nX = (int)(this.m_dX - m_dTorpedoWidth);
            this.m_nY = (int)(this.m_dY - m_dTorpedoHeight);
            graphics.drawImage(m_imageTorpedo, this.m_nX, this.m_nY, null);
            int n = this.m_nX - (int)EnemyTorpedo.m_dWakeWidth;
            int n2 = this.m_nY + 3;
            graphics.drawImage(EnemyTorpedo.m_imageWake[(Seafox.m_random.nextInt() & 0x7FFFFFF) % 4], n, n2, null);
            return;
        }
        if (this.m_nStatus == 2) {
            int n = (Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4;
            graphics.drawImage(VerticalTorpedo.m_imageBlast[n], this.m_nBlastX, this.m_nBlastY, null);
            return;
        }
    }

    public boolean CollisionAnalysis(double d, double d2, double d3, double d4) {
        if (this.m_nStatus == 1) {
            double d5 = this.m_dX - m_dTorpedoWidth;
            double d6 = this.m_dY - m_dTorpedoHeight / 2.0;
            double d7 = this.m_dX;
            double d8 = this.m_dY + m_dTorpedoHeight;
            if (d <= d7 && d3 >= d5 && d2 <= d8 && d4 >= d6) {
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                Seafox.m_bPlayLittleExplosion = true;
                return true;
            }
        }
        return false;
    }

    HorizontalTorpedo() {
    }

    public void Update() {
        if (this.m_nStatus == 0) {
            return;
        }
        double d = (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
        if (this.m_nStatus == 1) {
            this.m_dX = this.m_dXstart + this.m_dXvelocity * d;
            this.m_dY = this.m_dYstart + this.m_dYvelocity * d;
            if (this.m_dY < Player.m_dYmin) {
                this.m_dY = Player.m_dYmin;
            }
            if (this.m_dX > (double)Seafox.m_nPlayfieldWidth + m_dTorpedoWidth + EnemyTorpedo.m_dWakeWidth) {
                this.m_nStatus = 0;
                return;
            }
            int n = 0;
            do {
                if (!Seafox.m_enemySub[n].CollisionAnalysis(this.m_dX, this.m_dY)) continue;
                this.m_nStatus = 0;
                return;
            } while (++n < 3);
            if (Seafox.m_package.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            }
            if (Seafox.m_dolphin.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                Seafox.m_killerFish.Launch();
                return;
            }
            double d2 = this.m_dY + m_dTorpedoHeight;
            double d3 = this.m_dX - m_dTorpedoWidth / 2.0;
            double d4 = this.m_dX + m_dTorpedoWidth / 2.0;
            int n2 = 0;
            do {
                if (Seafox.m_depthCharge[n2].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_depthCharge[n2].CollisionAnalysis(d3, this.m_dY, d4, d2)) continue;
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            } while (++n2 < 3);
            n2 = 0;
            do {
                if (Seafox.m_magneticMine[n2].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_magneticMine[n2].CollisionAnalysis(d3, this.m_dY, d4, d2)) continue;
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            } while (++n2 < 3);
            if (Seafox.m_verticalTorpedo.CollisionAnalysis(d3, this.m_dY, d4, d2)) {
                this.m_nBlastX = (int)this.m_dX - VerticalTorpedo.m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - VerticalTorpedo.m_nBlastHeight / 2;
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                return;
            }
            if (Seafox.m_supplySub.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nStatus = 0;
                return;
            }
        }
        if (this.m_nStatus == 2 && (int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) >= 500) {
            this.m_nStatus = 0;
            return;
        }
    }
}
