/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

class Destroyer {
    public static Image m_imageShip;
    public static double m_dShipWidth;
    public static int m_nShipHeight;
    public static final int Y_WATERLINE = 55;
    public static final int WAKE_IMAGES = 4;
    public static Image[] m_imageWake;
    public static int m_nWakeWidth;
    public static int m_nWakeHeight;
    public static final int MAX_SHIPS = 3;
    public static int m_nShipLimit;
    public static int m_nActiveShips;
    static final int MIN_LAUNCH_INTERVAL = 10000;
    static final int MAX_LAUNCH_INTERVAL = 15000;
    public static long m_lTimeNextLaunch;
    public static final double MIN_VELOCITY = 19.0;
    public static final double MAX_VELOCITY = 21.0;
    public static final int POINTS_FOR_SINKING = 250;
    static final double LEFT_CHARGE_MARGIN = 0.0;
    static final double RIGHT_CHARGE_MARGIN = 40.0;
    public int m_nStatus;
    static final int NORMAL = 0;
    static final int EXPLODING = 1;
    static final int SINKING = 2;
    static final int SCORING = 3;
    public static final int INACTIVE = 4;
    long m_lTimeStatusUpdated;
    long m_lTimeDepthCharge;
    DepthCharge m_depthCharge;
    double m_dXlaunch;
    public double m_dX;
    double m_dY;
    double m_dVelocity;

    public void Draw(Graphics graphics) {
        int n = 0;
        int n2 = 0;
        Image image = null;
        if (this.m_nStatus == 4) {
            return;
        }
        if (this.m_nStatus == 0 || this.m_nStatus == 1) {
            n = (int)(this.m_dX + 0.5);
            n2 = (int)this.m_dY;
            graphics.drawImage(m_imageShip, n, n2, null);
            if (this.m_nStatus == 0) {
                n2 = 53;
                image = m_imageWake[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
                graphics.drawImage(image, n += (int)m_dShipWidth - 4, n2, null);
                return;
            }
            image = CapitalShip.m_imageBlast[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
            graphics.drawImage(image, n += ((int)m_dShipWidth - image.getWidth(null)) / 2, n2 += (m_nShipHeight - image.getHeight(null)) / 2, null);
            return;
        }
        if (this.m_nStatus == 2) {
            int n3 = (int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) * 4 / 999;
            image = CapitalShip.m_imageSink[n3];
            n = (int)this.m_dX + ((int)m_dShipWidth - image.getWidth(null)) / 2;
            n2 = 55 - image.getHeight(null);
            graphics.drawImage(image, n, n2, null);
            return;
        }
        if (this.m_nStatus == 3) {
            graphics.setFont(Seafox.m_fontScorePopup);
            graphics.setColor(Color.yellow);
            String string = new String("" + 250);
            n = (int)this.m_dX + 10;
            graphics.drawString(string, n, 55);
        }
    }

    public void Launch() {
        this.m_nStatus = 0;
        this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
        ++m_nActiveShips;
        this.m_dX = this.m_dXlaunch = (double)Seafox.m_nPlayfieldWidth;
        this.m_dY = 55 - m_nShipHeight;
        this.m_dVelocity = -(19.0 + 2.0 * Seafox.m_random.nextDouble());
        m_lTimeNextLaunch = this.m_lTimeStatusUpdated + 10000L + (long)(5000.0 * Seafox.m_random.nextDouble());
        this.m_lTimeDepthCharge = 0L;
        this.m_depthCharge = null;
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (this.m_nStatus == 0 && d2 <= 55.0 && d2 >= 45.0 && d >= this.m_dX + 5.0 && d <= this.m_dX + m_dShipWidth - 5.0) {
            this.m_nStatus = 1;
            this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
            Seafox.m_nScore += 250;
            Seafox.m_bPlayBigExplosion = true;
            if (this.m_depthCharge != null && this.m_depthCharge.m_nStatus == 1 && this.m_depthCharge.m_destroyer.m_dX == this.m_dX) {
                this.m_depthCharge.Detonate();
            }
            return true;
        }
        return false;
    }

    static {
        m_imageWake = new Image[4];
    }

    public void Update() {
        if (this.m_nStatus == 4) {
            if (m_nActiveShips < m_nShipLimit && Seafox.m_lLatestTime >= m_lTimeNextLaunch) {
                this.Launch();
            }
            return;
        }
        if (this.m_nStatus == 0) {
            this.m_dX = this.m_dXlaunch + this.m_dVelocity * (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
            if (this.m_dX < -(m_dShipWidth + (double)m_nWakeWidth)) {
                this.m_nStatus = 4;
                m_nActiveShips += -1;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
            }
            if (this.m_dX >= 0.0 && this.m_dX <= (double)Seafox.m_nPlayfieldWidth - m_dShipWidth - 40.0 && Seafox.m_lLatestTime >= this.m_lTimeDepthCharge && DepthCharge.m_nActiveCharges < DepthCharge.m_nChargeLimit) {
                int n = 0;
                n = 0;
                while (Seafox.m_depthCharge[n].m_nStatus != 0 && ++n < 3) {
                }
                if (n < 3) {
                    this.m_depthCharge = Seafox.m_depthCharge[n];
                    this.m_lTimeDepthCharge = Seafox.m_lLatestTime + 6000L + (long)(Seafox.m_random.nextDouble() * 2000.0);
                    this.m_depthCharge.Launch(this);
                }
            }
            return;
        }
        if (this.m_nStatus == 1) {
            if ((int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) >= 500) {
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
            }
            return;
        }
        if (this.m_nStatus == 2) {
            if ((int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) >= 999) {
                this.m_nStatus = 3;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
            }
            return;
        }
        if (this.m_nStatus == 3) {
            if ((int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) >= 999) {
                this.m_nStatus = 4;
                m_nActiveShips += -1;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
            }
            return;
        }
    }

    Destroyer() {
    }
}
