/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Graphics;
import java.awt.Image;

class VerticalTorpedo {
    public static Image m_imageUp;
    public static Image m_imageDown;
    public static final int WAKE_IMAGES = 4;
    public static Image[] m_imageUpWake;
    public static Image[] m_imageDownWake;
    public static final int BLAST_IMAGES = 4;
    public static Image[] m_imageBlast;
    public static int m_nBlastWidth;
    public static int m_nBlastHeight;
    int m_nBlastX;
    int m_nBlastY;
    long m_lBlastStartTime;
    public static final int BLAST_DURATION = 500;
    public static int m_nTorpedoWidth;
    public static int m_nTorpedoHeight;
    static final double Y_MARGIN = 30.0;
    static final double VELOCITY = 24.0;
    long m_lStartTime;
    double m_dYstart;
    static final double X_FIRE_OFFSET = 45.0;
    static final double Y_FIRE_OFFSET = 0.0;
    double m_dX;
    double m_dY;
    int m_nX;
    int m_nY;
    public static final int INACTIVE = 0;
    static final int MOVING_UP = 1;
    static final int MOVING_DOWN = 2;
    public static final int EXPLODING = 3;
    public int m_nStatus;

    public void Fire(Player player) {
        this.m_lStartTime = Seafox.m_lLatestTime;
        this.m_dX = Player.m_dX + 45.0;
        this.m_dY = this.m_dYstart = Player.m_dY + 0.0;
        this.m_nStatus = 1;
        Seafox.m_bPlayFireTorpedo = true;
    }

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            this.m_nX = (int)this.m_dX - m_nTorpedoWidth / 2;
            this.m_nY = (int)(this.m_dY + 0.5);
            graphics.drawImage(m_imageUp, this.m_nX, this.m_nY, null);
            int n = (int)this.m_dX;
            int n2 = (int)this.m_dY + m_nTorpedoHeight;
            graphics.drawImage(m_imageUpWake[(Seafox.m_random.nextInt() & 0x7FFFFFF) % 4], n, n2, null);
            return;
        }
        if (this.m_nStatus == 2) {
            this.m_nX = (int)this.m_dX - m_nTorpedoWidth / 2;
            this.m_nY = (int)(this.m_dY + 0.5) - m_nTorpedoHeight;
            graphics.drawImage(m_imageDown, this.m_nX, this.m_nY, null);
            int n = (int)this.m_dX;
            int n3 = (int)this.m_dY - m_nTorpedoHeight - 15;
            graphics.drawImage(m_imageDownWake[(Seafox.m_random.nextInt() & 0x7FFFFFF) % 4], n, n3, null);
            return;
        }
        if (this.m_nStatus == 3) {
            int n = (Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4;
            graphics.drawImage(m_imageBlast[n], this.m_nBlastX, this.m_nBlastY, null);
            return;
        }
    }

    public boolean CollisionAnalysis(double d, double d2, double d3, double d4) {
        double d5 = 0.0;
        double d6 = 0.0;
        double d7 = 0.0;
        double d8 = 0.0;
        switch (this.m_nStatus) {
            case 1: {
                d5 = this.m_dX - (double)m_nTorpedoWidth / 2.0;
                d7 = this.m_dX + (double)m_nTorpedoWidth / 2.0;
                d6 = this.m_dY;
                d8 = this.m_dY + (double)m_nTorpedoHeight;
                break;
            }
            case 2: {
                d5 = this.m_dX - (double)m_nTorpedoWidth / 2.0;
                d7 = this.m_dX + (double)m_nTorpedoWidth / 2.0;
                d6 = this.m_dY - (double)m_nTorpedoHeight;
                d8 = this.m_dY;
                break;
            }
            default: {
                return false;
            }
        }
        if (d <= d7 && d3 >= d5 && d2 <= d8 && d4 >= d6) {
            this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
            this.m_nBlastY = (int)this.m_dY - m_nBlastHeight / 2;
            this.m_nStatus = 3;
            this.m_lBlastStartTime = Seafox.m_lLatestTime;
            Seafox.m_bPlayLittleExplosion = true;
            return true;
        }
        return false;
    }

    static {
        m_imageUpWake = new Image[4];
        m_imageDownWake = new Image[4];
        m_imageBlast = new Image[4];
    }

    public void Update() {
        int n;
        double d;
        double d2;
        int n2;
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1 || this.m_nStatus == 2) {
            if ((int)this.m_dY <= 55 && (int)this.m_dY >= 45) {
                n2 = 0;
                do {
                    if (!Seafox.m_destroyer[n2].CollisionAnalysis(this.m_dX, this.m_dY)) continue;
                    this.m_nStatus = 0;
                    return;
                } while (++n2 < 3);
            }
            n2 = 0;
            do {
                if (!Seafox.m_enemySub[n2].CollisionAnalysis(this.m_dX, this.m_dY)) continue;
                this.m_nStatus = 0;
                return;
            } while (++n2 < 3);
            if (Seafox.m_package.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - m_nBlastHeight / 2;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                Seafox.m_bPlayLittleExplosion = true;
                return;
            }
            if (Seafox.m_dolphin.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY - m_nBlastHeight / 2;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                Seafox.m_bPlayLittleExplosion = true;
                Seafox.m_killerFish.Launch();
                return;
            }
        }
        if (this.m_nStatus == 1) {
            this.m_dY = this.m_dYstart - (double)(Seafox.m_lLatestTime - this.m_lStartTime) * 24.0 / 1000.0;
            if (this.m_dY < -30.0) {
                this.m_nStatus = 0;
                return;
            }
            if ((int)this.m_dY <= 38 && (int)this.m_dY >= 28) {
                n2 = 0;
                do {
                    if (!Seafox.m_hospitalShip[n2].CollisionAnalysis(this.m_dX, this.m_dY)) continue;
                    this.m_nStatus = 2;
                    this.m_dY += (double)m_nTorpedoHeight;
                    this.m_dYstart = this.m_dY;
                    this.m_lStartTime = Seafox.m_lLatestTime;
                    Seafox.m_bPlayClank = true;
                    return;
                } while (++n2 < 3);
            }
            if ((int)this.m_dY <= 22 && (int)this.m_dY >= 12) {
                n2 = 0;
                do {
                    if (!Seafox.m_capitalShip[n2].CollisionAnalysis(this.m_dX, this.m_dY)) continue;
                    this.m_nStatus = 0;
                    return;
                } while (++n2 < 10);
            }
            double d3 = this.m_dY + (double)m_nTorpedoHeight;
            d2 = this.m_dX - (double)m_nTorpedoWidth / 2.0;
            d = this.m_dX + (double)m_nTorpedoWidth / 2.0;
            n = 0;
            do {
                if (Seafox.m_depthCharge[n].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_depthCharge[n].CollisionAnalysis(d2, this.m_dY, d, d3)) continue;
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
            n = 0;
            do {
                if (Seafox.m_magneticMine[n].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_magneticMine[n].CollisionAnalysis(d2, this.m_dY, d, d3)) continue;
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)this.m_dY;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
        }
        if (this.m_nStatus == 2) {
            this.m_dY = this.m_dYstart + (double)(Seafox.m_lLatestTime - this.m_lStartTime) * 24.0 / 1000.0;
            if (this.m_dY > (double)Seafox.m_nPlayfieldHeight + 30.0) {
                this.m_nStatus = 0;
                return;
            }
            if (Seafox.m_player.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nStatus = 0;
                return;
            }
            if (Seafox.m_supplySub.CollisionAnalysis(this.m_dX, this.m_dY)) {
                this.m_nStatus = 0;
                return;
            }
            double d4 = this.m_dY - (double)m_nTorpedoHeight;
            d2 = this.m_dX - (double)m_nTorpedoWidth / 2.0;
            d = this.m_dX + (double)m_nTorpedoWidth / 2.0;
            n = 0;
            do {
                if (Seafox.m_depthCharge[n].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_depthCharge[n].CollisionAnalysis(d2, d4, d, this.m_dY)) continue;
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)d4;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
            n = 0;
            do {
                if (Seafox.m_magneticMine[n].Detonate(this.m_dX, this.m_dY)) {
                    this.m_nStatus = 0;
                    return;
                }
                if (!Seafox.m_magneticMine[n].CollisionAnalysis(d2, d4, d, this.m_dY)) continue;
                this.m_nBlastX = (int)this.m_dX - m_nBlastWidth / 2;
                this.m_nBlastY = (int)d4;
                this.m_nStatus = 3;
                this.m_lBlastStartTime = Seafox.m_lLatestTime;
                return;
            } while (++n < 3);
        }
        if (this.m_nStatus == 3 && (int)(Seafox.m_lLatestTime - this.m_lBlastStartTime) >= 500) {
            this.m_nStatus = 0;
            return;
        }
    }

    VerticalTorpedo() {
    }
}
