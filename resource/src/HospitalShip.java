/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Graphics;
import java.awt.Image;

class HospitalShip {
    public static final int SHIPS = 3;
    public static final double VELOCITY = 24.0;
    public static final int Y_WATERLINE = 38;
    public static final int X_MARGIN = 40;
    public static int m_nXspacing;
    public static long m_lStartTime;
    public static double m_dXoriginStart;
    public static double m_dXoriginNow;
    public static Image m_imageShip;
    double m_dXoffset;
    int m_nXcenter;
    int m_nX;
    int m_nY;
    public boolean m_bHittable;

    public void Draw(Graphics graphics) {
        this.m_bHittable = false;
        double d = this.m_dXoffset + m_dXoriginNow;
        this.m_nXcenter = (int)(d + 0.5);
        if (this.m_nXcenter > 0) {
            this.m_nXcenter %= m_nXspacing * 3;
        }
        if (this.m_nXcenter > Seafox.m_nPlayfieldWidth + 40) {
            this.m_nXcenter -= m_nXspacing * 3;
        }
        if (this.m_nXcenter < -40) {
            return;
        }
        Image image = CapitalShip.m_imageWake[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
        this.m_nX = this.m_nXcenter - m_imageShip.getWidth(null) / 2;
        this.m_nY = 38 - m_imageShip.getHeight(null);
        int n = this.m_nX - CapitalShip.m_nWakeWidth + 5;
        int n2 = 38 - CapitalShip.m_nWakeHeight + 1;
        graphics.drawImage(image, n, n2, null);
        graphics.drawImage(m_imageShip, this.m_nX, this.m_nY, null);
        this.m_bHittable = true;
    }

    public boolean CollisionAnalysis(double d, double d2) {
        int n;
        return this.m_bHittable && d2 <= 38.0 && d2 >= 28.0 && (int)d >= this.m_nXcenter - (n = m_imageShip.getWidth(null) / 2 - 5) && (int)d <= this.m_nXcenter + n;
    }

    HospitalShip() {
    }
}
