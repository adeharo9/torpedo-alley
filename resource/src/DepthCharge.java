/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  Destroyer
 *  Player
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

class DepthCharge {
    public static final int BLAST_IMAGES = 4;
    public static final int BLAST_DURATION = 500;
    public static Image[] m_imageBlast = new Image[4];
    public static final int SPLASH_IMAGES = 4;
    static final int SPLASH_DURATION = 500;
    public static Image[] m_imageSplash = new Image[4];
    public static final int MAX_CHARGES = 3;
    public static int m_nChargeLimit;
    public static int m_nActiveCharges;
    static final int MIN_LAUNCH_INTERVAL = 6000;
    static final int MAX_LAUNCH_INTERVAL = 8000;
    static final Color COLOR;
    static final double LAUNCH_X = 35.0;
    static final double LAUNCH_Y = 8.0;
    static final double LAUNCH_DISTANCE = 25.0;
    static final double LAUNCH_VELOCITY = 20.0;
    static final int LAUNCH_SIZE = 2;
    static final double FALL_Y_ACCELERATION = 30.0;
    static final double MAX_X_VELOCITY = 6.0;
    static final double Y_VELOCITY = 20.0;
    static final double MAX_TUMBLE_RATE = 4.0;
    static final double MAJOR_AXIS = 8.0;
    static final double MINOR_AXIS = 4.0;
    static final double MIN_BLAST_RADIUS = 10.0;
    static final double MAX_BLAST_RADIUS = 25.0;
    public int m_nStatus;
    public static final int INACTIVE = 0;
    public static final int LAUNCHING = 1;
    static final int FALLING = 2;
    static final int TUMBLING = 3;
    static final int EXPLODING = 4;
    long m_lTimeStatusUpdated;
    double m_dYdetonate;
    public Destroyer m_destroyer;
    double m_dX0;
    double m_dY0;
    double m_dX;
    double m_dY;
    double m_dTumbleRate;
    double m_dXvelocity;
    double m_dAngle0;
    double m_dAngle;

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1 || this.m_nStatus == 2) {
            graphics.setColor(COLOR);
            graphics.fillRect((int)this.m_dX, (int)this.m_dY, 2, 2);
            return;
        }
        if (this.m_nStatus == 3) {
            int[] arrn = new int[4];
            int[] arrn2 = new int[4];
            double d = Math.cos(this.m_dAngle);
            double d2 = Math.sin(this.m_dAngle);
            arrn[0] = (int)(4.0 * d + 2.0 * d2 + this.m_dX + 0.5);
            arrn2[0] = (int)(4.0 * d2 - 2.0 * d + this.m_dY + 0.5);
            arrn[1] = (int)(4.0 * d - 2.0 * d2 + this.m_dX + 0.5);
            arrn2[1] = (int)(4.0 * d2 + 2.0 * d + this.m_dY + 0.5);
            arrn[2] = (int)(-4.0 * d - 2.0 * d2 + this.m_dX + 0.5);
            arrn2[2] = (int)(-4.0 * d2 + 2.0 * d + this.m_dY + 0.5);
            arrn[3] = (int)(-4.0 * d + 2.0 * d2 + this.m_dX + 0.5);
            arrn2[3] = (int)(-4.0 * d2 - 2.0 * d + this.m_dY + 0.5);
            graphics.setColor(COLOR);
            graphics.fillPolygon(arrn, arrn2, 4);
            if (Seafox.m_lLatestTime - this.m_lTimeStatusUpdated < 500L) {
                Image image = m_imageSplash[(int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) * 4 / 500];
                int n = (int)this.m_dX - image.getWidth(null) / 2;
                int n2 = 58 - image.getHeight(null);
                graphics.drawImage(image, n, n2, null);
            }
            return;
        }
        if (this.m_nStatus == 4) {
            Image image = m_imageBlast[(int)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) * 4 / 500];
            int n = (int)this.m_dX - image.getWidth(null) / 2;
            int n3 = (int)this.m_dY - image.getHeight(null) / 2;
            graphics.drawImage(image, n, n3, null);
            return;
        }
    }

    public void Launch(Destroyer destroyer) {
        this.m_nStatus = 1;
        this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
        this.m_destroyer = destroyer;
        this.m_dX = destroyer.m_dX + 35.0;
        this.m_dY = destroyer.m_dY + 8.0;
        this.m_dYdetonate = Player.m_dY + Player.m_dSubHeight / 2.0;
        ++m_nActiveCharges;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3, double d4) {
        double d5;
        if (this.m_nStatus == 4 && d3 > this.m_dX - (d5 = 10.0 + 15.0 * (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 500.0) && d < this.m_dX + d5 && d4 > this.m_dY - d5 && d2 < this.m_dY + d5) {
            double d6 = 0.0;
            d6 = Math.abs(this.m_dY - d4) < Math.abs(this.m_dY - d2) ? Math.abs(this.m_dY - d4) : Math.abs(this.m_dY - d2);
            double d7 = Math.acos(d6 / d5);
            double d8 = Math.abs(d5 * Math.sin(d7));
            if (d3 >= this.m_dX - d8 && d <= this.m_dX + d8) {
                return true;
            }
        }
        return false;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3) {
        if (this.m_nStatus == 3) {
            double d4 = 3.0;
            double d5 = Math.abs(this.m_dX + d4 - d);
            double d6 = Math.abs(this.m_dY + d4 - d2);
            double d7 = d3 + d4;
            if (d5 <= d7 && d6 <= d7 && Math.sqrt(d5 * d5 + d6 * d6) <= d7) {
                this.Detonate();
                return true;
            }
        }
        return false;
    }

    static {
        COLOR = new Color(255, 128, 0);
    }

    DepthCharge() {
    }

    public void Update() {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            double d = 20.0 * (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
            this.m_dX = this.m_destroyer.m_dX + d + 35.0;
            if (d >= 25.0) {
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                this.m_dX0 = this.m_dX;
                this.m_dY0 = this.m_dY;
            }
            return;
        }
        if (this.m_nStatus == 2) {
            double d = (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
            this.m_dX = this.m_dX0 + (20.0 + this.m_destroyer.m_dVelocity) * d;
            this.m_dY = this.m_dY0 + 15.0 * d;
            if (this.m_dY >= 55.0) {
                this.m_nStatus = 3;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                this.m_dX0 = this.m_dX;
                this.m_dY0 = this.m_dY;
                this.m_dAngle0 = 3.1415926536 * Seafox.m_random.nextDouble();
                this.m_dTumbleRate = 2.0 * (Seafox.m_random.nextDouble() - 0.5) * 4.0;
                this.m_dXvelocity = 2.0 * (Seafox.m_random.nextDouble() - 0.5) * 6.0;
                Seafox.m_bPlaySplash = true;
            }
            return;
        }
        if (this.m_nStatus == 3) {
            double d = (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 1000.0;
            this.m_dX = this.m_dX0 + this.m_dXvelocity * d;
            this.m_dY = this.m_dY0 + 20.0 * d;
            this.m_dAngle = this.m_dAngle0 + this.m_dTumbleRate * d;
            if (this.m_dY >= this.m_dYdetonate) {
                this.Detonate();
            }
            return;
        }
        if (this.m_nStatus == 4) {
            if (Seafox.m_lLatestTime - this.m_lTimeStatusUpdated >= 500L) {
                this.m_nStatus = 0;
                this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
                m_nActiveCharges += -1;
                return;
            }
            double d = 10.0 + 15.0 * (double)(Seafox.m_lLatestTime - this.m_lTimeStatusUpdated) / 500.0;
            int n = 0;
            do {
                Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX, this.m_dY, d);
            } while (++n < 3);
        }
    }

    public void Detonate() {
        this.m_nStatus = 4;
        this.m_lTimeStatusUpdated = Seafox.m_lLatestTime;
        Seafox.m_bPlayBigExplosion = true;
    }

    public boolean Detonate(double d, double d2) {
        double d3;
        if (this.m_nStatus == 3 && d >= this.m_dX - (d3 = 6.0) && d <= this.m_dX + d3 && d2 >= this.m_dY - d3 && d2 <= this.m_dY + d3) {
            this.Detonate();
            return true;
        }
        return false;
    }

    public boolean Detonate(double d, double d2, double d3, double d4) {
        double d5;
        if (this.m_nStatus == 4) {
            return this.CollisionAnalysis(d, d2, d3, d4);
        }
        if (this.m_nStatus == 3 && d3 >= this.m_dX - (d5 = 6.0) && d <= this.m_dX + d5 && d4 >= this.m_dY - d5 && d2 <= this.m_dY + d5) {
            this.Detonate();
            return true;
        }
        return false;
    }
}
