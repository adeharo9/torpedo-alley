/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  Player
 */
import java.awt.Graphics;
import java.awt.Image;

class KillerFish {
    public static Image m_imageFish;
    static final double X_OFFSET = -65.0;
    static final double Y_OFFSET = -24.0;
    double m_dX;
    double m_dY;
    double m_dXstart;
    long m_lStartTime;
    static final double VELOCITY = 50.0;
    public boolean m_bActive;

    public void Draw(Graphics graphics) {
        if (!this.m_bActive) {
            return;
        }
        graphics.drawImage(m_imageFish, (int)this.m_dX, (int)this.m_dY, null);
    }

    public void Launch() {
        this.m_bActive = true;
        this.m_dXstart = -((double)m_imageFish.getWidth(null));
        this.m_lStartTime = Seafox.m_lLatestTime;
        this.m_dX = this.m_dXstart;
        this.m_dY = Player.m_dY + -24.0;
        Seafox.m_bPlayShark = true;
    }

    KillerFish() {
    }

    public void Update() {
        if (!this.m_bActive) {
            return;
        }
        this.m_dX = this.m_dXstart + 50.0 * (double)(Seafox.m_lLatestTime - this.m_lStartTime) / 1000.0;
        this.m_dY = Player.m_dY + -24.0;
        if (this.m_dX >= Player.m_dX + -65.0 && Player.m_nStatus == 0) {
            Player.m_nStatus = 5;
        }
        if (this.m_dX >= (double)Seafox.m_nPlayfieldWidth) {
            if (Player.m_nStatus == 5) {
                Player.m_nStatus = 2;
            }
            this.m_bActive = false;
        }
    }
}
