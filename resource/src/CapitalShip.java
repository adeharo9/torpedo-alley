/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

class CapitalShip {
    public static final int SHIPS = 10;
    public static final int SHIP_IMAGES = 4;
    public static final int WAKE_IMAGES = 4;
    public static final int BLAST_IMAGES = 4;
    public static final int SINK_IMAGES = 4;
    public static final int BLAST_TIME = 500;
    public static final int SINK_TIME = 999;
    public static final int SCORE_TIME = 999;
    public static final int Y_WATERLINE = 22;
    public static final int X_MARGIN = 40;
    public static final double VELOCITY = 20.0;
    public static int m_nXspacing;
    public static int m_nShipsSunk;
    public static long m_lStartTime;
    public static double m_dXoriginStart;
    public static double m_dXoriginNow;
    public static int m_nWakeWidth;
    public static int m_nWakeHeight;
    public static Image[] m_imageShip;
    public static Image[] m_imageWake;
    public static Image[] m_imageBlast;
    public static Image[] m_imageSink;
    public boolean m_bSinkable;
    double m_dXoffset;
    int m_ndxImage;
    static final int NORMAL = 0;
    static final int SINKING = 1;
    static final int SUNK = 2;
    int m_nStatus;
    int m_nPoints;
    int m_nXcenter;
    int m_nX;
    int m_nY;
    long m_lTimeHit;

    public void Draw(Graphics graphics) {
        Image image;
        int n;
        int n2;
        this.m_bSinkable = false;
        if (this.m_nStatus == 2) {
            return;
        }
        if (this.m_nStatus == 0) {
            double d = this.m_dXoffset + m_dXoriginNow;
            this.m_nXcenter = (int)(d + 0.5);
            if (this.m_nXcenter > 0) {
                this.m_nXcenter %= m_nXspacing * 10;
            }
            if (this.m_nXcenter > Seafox.m_nPlayfieldWidth + 40) {
                this.m_nXcenter -= m_nXspacing * 10;
            }
            if (this.m_nXcenter < -40) {
                return;
            }
        }
        if (this.m_nStatus == 1 && (int)(Seafox.m_lLatestTime - this.m_lTimeHit) > 2498) {
            this.m_nStatus = 2;
            return;
        }
        if (this.m_nStatus == 0 || this.m_nStatus == 1 && (int)(Seafox.m_lLatestTime - this.m_lTimeHit) < 500) {
            Image image2 = m_imageShip[this.m_ndxImage];
            this.m_nX = this.m_nXcenter - image2.getWidth(null) / 2;
            this.m_nY = 22 - image2.getHeight(null);
            if (this.m_nStatus == 0) {
                n2 = this.m_nX - m_nWakeWidth + 5;
                n = 22 - m_nWakeHeight + 1;
                image = m_imageWake[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
                graphics.drawImage(image, n2, n, null);
            }
            graphics.drawImage(image2, this.m_nX, this.m_nY, null);
        }
        if (this.m_nStatus == 0) {
            this.m_bSinkable = true;
            return;
        }
        int n3 = (int)(Seafox.m_lLatestTime - this.m_lTimeHit);
        if (n3 <= 500) {
            n2 = this.m_nXcenter - 20;
            n = 0;
            image = m_imageBlast[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
            graphics.drawImage(image, n2, n, null);
            return;
        }
        if (n3 <= 1499) {
            n2 = (n3 - 500) / 249;
            Image image3 = m_imageSink[n2];
            int n4 = this.m_nXcenter - image3.getWidth(null) / 2;
            int n5 = 22 - image3.getHeight(null);
            graphics.drawImage(image3, n4, n5, null);
            return;
        }
        graphics.setFont(Seafox.m_fontScorePopup);
        graphics.setColor(Color.yellow);
        String string = new String("" + this.m_nPoints);
        n = this.m_nXcenter - 20;
        if (this.m_nPoints >= 1000) {
            n -= 7;
        }
        graphics.drawString(string, n, 22);
    }

    public boolean CollisionAnalysis(double d, double d2) {
        int n;
        if (this.m_bSinkable && d2 <= 22.0 && d2 >= 12.0 && (int)d >= this.m_nXcenter - (n = m_imageShip[this.m_ndxImage].getWidth(null) / 2 - 5) && (int)d <= this.m_nXcenter + n) {
            this.m_nStatus = 1;
            this.m_lTimeHit = Seafox.m_lLatestTime;
            this.m_nPoints = (Seafox.m_nMission - 1) * 1000 + ++m_nShipsSunk * 100;
            Seafox.m_nScore += this.m_nPoints;
            Seafox.m_bPlayBigExplosion = true;
            if (m_nShipsSunk == 10) {
                Seafox.m_player.MissionComplete();
            }
            return true;
        }
        return false;
    }

    static {
        m_imageShip = new Image[4];
        m_imageWake = new Image[4];
        m_imageBlast = new Image[4];
        m_imageSink = new Image[4];
    }

    CapitalShip() {
    }
}
