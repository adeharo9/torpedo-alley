/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Graphics;
import java.awt.Image;

class Player {
    public static Image m_imageSub;
    public static int m_nStatus;
    public static final int NORMAL = 0;
    static final int EXPLODING = 1;
    public static final int DESTROYED = 2;
    public static final int MISSION_COMPLETE = 3;
    public static final int ENTERING = 4;
    public static final int SWALLOWED = 5;
    static long m_lExplosionStartTime;
    static final int EXPLOSION_DURATION = 500;
    static double m_dSubWidth;
    static double m_dSubHeight;
    static double m_dXvelocity;
    public static double m_dYvelocity;
    static final double MAX_VELOCITY = 27.0;
    static final int PLAYER_TOP_MARGIN = 15;
    static final int PLAYER_BOTTOM_MARGIN = 0;
    public long m_lRestartTime;
    static final long RUNOUT_TIME = 2000L;
    long m_lMissionCompleteTime;
    double m_dXmissionComplete;
    static final double MISSION_COMPLETE_VELOCITY = 50.0;
    long m_lEnteringTime;
    static final double ENTERING_VELOCITY = 50.0;
    static final double CONNING_TOWER_LEFT = 19.0;
    static final double CONNING_TOWER_RIGHT = 31.0;
    static final double CONNING_TOWER_TOP = 0.0;
    static final double CONNING_TOWER_BOTTOM = 8.0;
    static final double HULL_LEFT = 1.0;
    static final double HULL_RIGHT = 57.0;
    static final double HULL_TOP = 8.0;
    static final double HULL_BOTTOM = 20.0;
    public static double m_dX;
    public static double m_dY;
    static double m_dXmin;
    static double m_dXmax;
    public static double m_dYmin;
    static double m_dYmax;
    public static double m_dFuel;
    long m_lTimeMaxFuel;
    long m_lTimeNoFuel;
    double m_dYnoFuel;
    public static final double MAX_FUEL = 100.0;
    static final double FUEL_USE_RATE = 0.8;
    static final double FREEFALL_RATE = 50.0;
    public static int m_nTorpedoes;
    public static final int MAX_TORPEDOES = 10;

    public void Draw(Graphics graphics) {
        if (m_nStatus == 2 || m_nStatus == 5) {
            return;
        }
        int n = (int)(m_dX + 0.5);
        int n2 = (int)(m_dY + 0.5);
        graphics.drawImage(m_imageSub, n, n2, null);
        if (m_nStatus == 1) {
            Image image = CapitalShip.m_imageBlast[(Seafox.m_random.nextInt() & Integer.MAX_VALUE) % 4];
            int n3 = (int)(m_dX + (m_dSubWidth - (double)image.getWidth(null)) / 2.0);
            int n4 = (int)(m_dY + (m_dSubHeight - (double)image.getHeight(null)) / 2.0);
            graphics.drawImage(image, n3, n4, null);
        }
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (m_nStatus == 0 && d >= m_dX && d <= m_dX + m_dSubWidth && d2 >= m_dY && d2 <= m_dY + m_dSubHeight) {
            double d3 = d - m_dX;
            double d4 = d2 - m_dY;
            if (d3 >= 19.0 && d3 <= 31.0 && d4 >= 0.0 && d4 <= 8.0 || d3 >= 1.0 && d3 <= 57.0 && d4 >= 8.0 && d4 <= 20.0) {
                m_nStatus = 1;
                m_lExplosionStartTime = Seafox.m_lLatestTime;
                Seafox.m_bPlayBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    public boolean CollisionAnalysis(EnemySub enemySub) {
        if (m_nStatus == 0 && m_dX <= enemySub.m_dX + EnemySub.m_dSubWidth && m_dX + m_dSubWidth >= enemySub.m_dX && m_dY <= enemySub.m_dY + EnemySub.m_dSubHeight && m_dY + m_dSubHeight >= enemySub.m_dY) {
            double d = enemySub.m_dX - m_dX;
            double d2 = enemySub.m_dY - m_dY;
            if (19.0 <= d + 31.0 && 31.0 >= d + 19.0 && 0.0 <= d2 + 8.0 && 8.0 >= d2 + 0.0 || 19.0 <= d + 57.0 && 31.0 >= d + 1.0 && 0.0 <= d2 + 20.0 && 8.0 >= d2 + 8.0 || 1.0 <= d + 31.0 && 57.0 >= d + 19.0 && 8.0 <= d2 + 8.0 && 20.0 >= d2 + 0.0 || 1.0 <= d + 57.0 && 57.0 >= d + 1.0 && 8.0 <= d2 + 20.0 && 20.0 >= d2 + 8.0) {
                m_nStatus = 1;
                m_lExplosionStartTime = Seafox.m_lLatestTime;
                Seafox.m_bPlayBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    public boolean CollisionAnalysis(Package package_) {
        if (m_nStatus == 0 && m_dX <= package_.m_dX + Package.m_dPackageWidth && m_dX + m_dSubWidth >= package_.m_dX && m_dY <= package_.m_dY + Package.m_dPackageHeight && m_dY + m_dSubHeight >= package_.m_dY) {
            double d = package_.m_dX - m_dX;
            double d2 = d + Package.m_dPackageWidth;
            double d3 = package_.m_dY - m_dY;
            double d4 = d3 + Package.m_dPackageHeight;
            if (d <= 31.0 && d2 >= 19.0 && d3 <= 8.0 && d4 >= 0.0 || d <= 57.0 && d2 >= 1.0 && d3 <= 20.0 && d4 >= 8.0) {
                m_nTorpedoes = 10;
                m_dFuel = 100.0;
                this.m_lTimeMaxFuel = Seafox.m_lLatestTime;
                this.m_lTimeNoFuel = 0L;
                Seafox.m_bPlaySupplies = true;
                return true;
            }
        }
        return false;
    }

    public void MissionInitialization() {
        m_nStatus = 4;
        this.m_lEnteringTime = System.currentTimeMillis();
        m_dY = Seafox.m_nPlayfieldHeight * 2 / 3;
        m_nTorpedoes = 10;
        m_dFuel = 100.0;
        this.m_lRestartTime = 0L;
    }

    static {
        m_dFuel = 100.0;
        m_nTorpedoes = 10;
    }

    public void OneTimeInitialization() {
        m_dSubWidth = m_imageSub.getWidth(null);
        m_dSubHeight = m_imageSub.getHeight(null);
        m_dXmax = (double)Seafox.m_nPlayfieldWidth - m_dSubWidth;
        m_dYmin = 75.0;
        m_dYmax = Seafox.m_nPlayfieldHeight - (0 + (int)m_dSubHeight);
        m_dX = (m_dXmax + m_dXmin) / 2.0;
        m_dY = 2.0 * (m_dYmax + m_dYmin) / 3.0;
    }

    public void MissionComplete() {
        Seafox.m_bMissionComplete = true;
        if (m_nStatus != 2) {
            m_nStatus = 3;
            this.m_dXmissionComplete = m_dX;
            this.m_lMissionCompleteTime = Seafox.m_lLatestTime;
        }
    }

    public void Update() {
        if (m_nStatus == 5) {
            return;
        }
        if (m_nStatus == 2) {
            if (this.m_lRestartTime == 0L) {
                Seafox.m_nSubsLeft += -1;
                this.m_lRestartTime = Seafox.m_lLatestTime + 2000L;
            }
            return;
        }
        if (m_nStatus == 1) {
            if ((int)(Seafox.m_lLatestTime - m_lExplosionStartTime) > 500) {
                m_nStatus = 2;
            }
            return;
        }
        if (m_nStatus == 3) {
            m_dX = this.m_dXmissionComplete + 50.0 * (double)(Seafox.m_lLatestTime - this.m_lMissionCompleteTime) / 1000.0;
            if (m_dX > (double)Seafox.m_nPlayfieldWidth && this.m_lRestartTime == 0L) {
                this.m_lRestartTime = Seafox.m_lLatestTime + 2000L;
            }
            return;
        }
        if (m_nStatus == 4) {
            m_dX = -m_dSubWidth + 50.0 * (double)(Seafox.m_lLatestTime - this.m_lEnteringTime) / 1000.0;
            if (m_dX >= ((double)Seafox.m_nPlayfieldWidth - m_dSubWidth) / 2.0) {
                m_nStatus = 0;
                m_nTorpedoes = 10;
                m_dFuel = 100.0;
                this.m_lTimeMaxFuel = System.currentTimeMillis();
                this.m_lTimeNoFuel = 0L;
                Seafox.m_bDisplayMissionTitle = false;
            }
            return;
        }
        m_dFuel = 100.0 - 0.8 * (double)(Seafox.m_lLatestTime - this.m_lTimeMaxFuel) / 1000.0;
        if (m_dFuel < 0.0) {
            m_dFuel = 0.0;
            if (this.m_lTimeNoFuel == 0L) {
                this.m_lTimeNoFuel = Seafox.m_lLatestTime;
                this.m_dYnoFuel = m_dY;
                return;
            }
            m_dY = this.m_dYnoFuel + 50.0 * (double)(Seafox.m_lLatestTime - this.m_lTimeNoFuel) / 1000.0;
            if (m_dY >= (double)Seafox.m_nPlayfieldHeight) {
                m_nStatus = 2;
            }
            return;
        }
        if (Seafox.m_bMouseInWindow) {
            double d = (double)Seafox.m_nMouseX - (m_dX + m_dSubWidth / 2.0);
            double d2 = (double)Seafox.m_nMouseY - (m_dY + m_dSubHeight / 2.0);
            if (d != 0.0 || d2 != 0.0) {
                if (Math.abs(d) >= Math.abs(d2)) {
                    double d3;
                    if (d > 0.0) {
                        m_dXvelocity = 27.0;
                        d3 = m_dXvelocity * Seafox.m_dSecondsPerFrame;
                        if (d3 > d) {
                            m_dXvelocity *= d / d3;
                        }
                    } else {
                        m_dXvelocity = -27.0;
                        d3 = m_dXvelocity * Seafox.m_dSecondsPerFrame;
                        if (d3 < d) {
                            m_dXvelocity *= d / d3;
                        }
                    }
                    m_dYvelocity = d2 * m_dXvelocity / d;
                } else {
                    double d4;
                    if (d2 > 0.0) {
                        m_dYvelocity = 27.0;
                        d4 = m_dYvelocity * Seafox.m_dSecondsPerFrame;
                        if (d4 > d2) {
                            m_dYvelocity *= d2 / d4;
                        }
                    } else {
                        m_dYvelocity = -27.0;
                        d4 = m_dXvelocity * Seafox.m_dSecondsPerFrame;
                        if (d4 < d2) {
                            m_dYvelocity *= d2 / d4;
                        }
                    }
                    m_dXvelocity = d * m_dYvelocity / d2;
                }
            } else {
                m_dXvelocity = 0.0;
                m_dYvelocity = 0.0;
            }
        } else {
            m_dXvelocity = 0.0;
            m_dYvelocity = 0.0;
        }
        m_dX += m_dXvelocity * Seafox.m_dSecondsPerFrame;
        if (m_dX > m_dXmax) {
            m_dX = m_dXmax;
            m_dXvelocity = 0.0;
        }
        if (m_dX < m_dXmin) {
            m_dX = m_dXmin;
            m_dXvelocity = 0.0;
        }
        if ((m_dY += m_dYvelocity * Seafox.m_dSecondsPerFrame) > m_dYmax) {
            m_dY = m_dYmax;
            m_dYvelocity = 0.0;
        }
        if (m_dY < m_dYmin) {
            m_dY = m_dYmin;
            m_dYvelocity = 0.0;
        }
        int n = 0;
        do {
            if (!Seafox.m_depthCharge[n].Detonate(m_dX + 19.0, m_dY + 0.0, m_dX + 31.0, m_dY + 8.0) && !Seafox.m_depthCharge[n].Detonate(m_dX + 1.0, m_dY + 8.0, m_dX + 57.0, m_dY + 20.0)) continue;
            m_nStatus = 1;
            m_lExplosionStartTime = Seafox.m_lLatestTime;
            Seafox.m_bPlayBigExplosion = true;
            return;
        } while (++n < 3);
        n = 0;
        do {
            if (!Seafox.m_magneticMine[n].Detonate(m_dX + 19.0, m_dY + 0.0, m_dX + 31.0, m_dY + 8.0) && !Seafox.m_magneticMine[n].Detonate(m_dX + 1.0, m_dY + 8.0, m_dX + 57.0, m_dY + 20.0)) continue;
            m_nStatus = 1;
            m_lExplosionStartTime = Seafox.m_lLatestTime;
            Seafox.m_bPlayBigExplosion = true;
            return;
        } while (++n < 3);
        if (Seafox.m_bButtonDown && Seafox.m_verticalTorpedo.m_nStatus == 0) {
            if (m_nTorpedoes > 0) {
                m_nTorpedoes += -1;
                Seafox.m_verticalTorpedo.Fire(this);
            } else {
                Seafox.m_bPlayOutOfTorpedoes = true;
                Seafox.m_bButtonDown = false;
            }
        }
        if (Seafox.m_bSpacebarPressed && Seafox.m_horizontalTorpedo.m_nStatus == 0) {
            if (m_nTorpedoes > 0) {
                m_nTorpedoes += -1;
                Seafox.m_horizontalTorpedo.Fire(this);
                return;
            }
            Seafox.m_bPlayOutOfTorpedoes = true;
            Seafox.m_bSpacebarPressed = false;
        }
    }

    Player() {
    }
}
