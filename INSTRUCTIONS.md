# How to Play

You start the game with three submarines. Guide your submarine with the mouse pointer. Press the mouse button to fire a torpedo upward toward the surface. Press the keyboard space bar to fire a torpedo to the right. Since it takes your crew some time to reload the torpedo tubes, you are allowed a maximum of one torpedo at a time in each direction.

As the submarine captain, you must destroy all ten of the merchant ships in the top row before you can move on to your next mission. Try not to torpedo hospital ships. It's a violation of international law--and the consequences can be pretty immediate. Skillful maneuvering will be necessary on advanced missions in order to survive exploding depth charges, hostile torpedo fire, and magnetic mines.

Your sub has a limited supply of fuel and torpedoes, and must be resupplied frequently. A supply sub will pass by occasionally near the ocean bottom and release a trained dolphin carrying fuel and torpedoes. By making contact with the dolphin's supply pack, you can resupply your vessel. Whatever you do, don't torpedo the dolphin! He has many friends in these waters, and they won't take too kindly to it.
