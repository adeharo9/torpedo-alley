package io.adeharo.torpedoalley;

// AWT: Abstract Window Toolkit, the foundation of later-to-be Swing (Java GUI framework)
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Explosive thrown to the water by destroyers that travels vertically and explodes once it reaches a certain depth
 */
class DepthCharge {
    // Static properties
    /**
     * Number of frames comprising the blast animation of a depth charge
     */
    public static final int BLAST_IMAGES = 4;
    /**
     * Duration of the blast animation, in ms
     */
    public static final int BLAST_DURATION = 500;
    /**
     * Blast animation frames
     */
    public static Image[] imageBlast = new Image[BLAST_IMAGES];
    /**
     * Number of frames comprising the splash animation of a depth charge when it falls into the water
     */
    public static final int SPLASH_IMAGES = 4;
    /**
     * Duration of the splash animation, in ms
     */
    static final int SPLASH_DURATION = 500;
    /**
     * Splash animation frames
     */
    public static Image[] imageSplash = new Image[4];
    /**
     * Maximum number of simultaneous depth charges
     * TODO: confirm
     */
    public static final int MAX_CHARGES = 3;
    /**
     * TODO: ???
     */
    public static int chargeLimit;
    /**
     * TODO: ???
     */
    public static int activeCharges;
    /**
     * Minimum interval between launching depth charges, in ms
     */
    static final int MIN_LAUNCH_INTERVAL = 6000;
    /**
     * Maximum interval between launching depth charges, in ms
     */
    static final int MAX_LAUNCH_INTERVAL = 8000;
    /**
     * TODO: color of what?
     */
    static final Color COLOR;
    /**
     * TODO: ??? Seems the x coordinate where it spawns wrt the destroyer?
     */
    static final double LAUNCH_X = 35.0;
    /**
     * TODO: ??? Seems the y coordinate where it spawns wrt the destroyer?
     */
    static final double LAUNCH_Y = 8.0;
    /**
     * TODO: ??? Seems the distance the charge travels in the destroyer before falling into the water?
     */
    static final double LAUNCH_DISTANCE = 25.0;
    /**
     * TODO: ???
     */
    static final double LAUNCH_VELOCITY = 20.0;
    static final int LAUNCH_SIZE = 2;
    static final double FALL_Y_ACCELERATION = 30.0;
    /**
     * Maximum speed on the x coordinate
     */
    static final double MAX_X_VELOCITY = 6.0;
    static final double Y_VELOCITY = 20.0;
    /**
     * Maximum rotation speed
     * TODO: in rotations/s?
     */
    static final double MAX_TUMBLE_RATE = 4.0;
    static final double MAJOR_AXIS = 8.0;
    static final double MINOR_AXIS = 4.0;
    /**
     *
     */
    static final double MIN_BLAST_RADIUS = 10.0;
    static final double MAX_BLAST_RADIUS = 25.0;
    public static final int INACTIVE = 0;
    public static final int LAUNCHING = 1;
    static final int FALLING = 2;
    static final int TUMBLING = 3;
    static final int EXPLODING = 4;
    /**
     * Current status of `this` depth charge
     */
    public int status;
    long timeStatusUpdated;
    double yDetonate;
    /**
     * Reference to the destroyer that is launching the depth charge
     */
    public Destroyer destroyer;
    double x0;
    double y0;
    double x;
    double y;
    double tumbleRate;
    double xVelocity;
    double angle0;
    double angle;

    public void Draw(Graphics graphics) {
        if (this.status == 0) {
            return;
        }
        if (this.status == 1 || this.status == 2) {
            graphics.setColor(COLOR);
            graphics.fillRect((int)this.x, (int)this.y, 2, 2);
            return;
        }
        if (this.status == 3) {
            int[] arrn = new int[4];
            int[] arrn2 = new int[4];
            double d = Math.cos(this.angle);
            double d2 = Math.sin(this.angle);
            arrn[0] = (int)(4.0 * d + 2.0 * d2 + this.x + 0.5);
            arrn2[0] = (int)(4.0 * d2 - 2.0 * d + this.y + 0.5);
            arrn[1] = (int)(4.0 * d - 2.0 * d2 + this.x + 0.5);
            arrn2[1] = (int)(4.0 * d2 + 2.0 * d + this.y + 0.5);
            arrn[2] = (int)(-4.0 * d - 2.0 * d2 + this.x + 0.5);
            arrn2[2] = (int)(-4.0 * d2 + 2.0 * d + this.y + 0.5);
            arrn[3] = (int)(-4.0 * d + 2.0 * d2 + this.x + 0.5);
            arrn2[3] = (int)(-4.0 * d2 - 2.0 * d + this.y + 0.5);
            graphics.setColor(COLOR);
            graphics.fillPolygon(arrn, arrn2, 4);
            if (Seafox.latestTime - this.timeStatusUpdated < 500L) {
                Image image = imageSplash[(int)(Seafox.latestTime - this.timeStatusUpdated) * 4 / 500];
                int n = (int)this.x - image.getWidth(null) / 2;
                int n2 = 58 - image.getHeight(null);
                graphics.drawImage(image, n, n2, null);
            }
            return;
        }
        if (this.status == 4) {
            Image image = imageBlast[(int)(Seafox.latestTime - this.timeStatusUpdated) * 4 / 500];
            int n = (int)this.x - image.getWidth(null) / 2;
            int n3 = (int)this.y - image.getHeight(null) / 2;
            graphics.drawImage(image, n, n3, null);
            return;
        }
    }

    public void Launch(Destroyer destroyer) {
        this.status = 1;
        this.timeStatusUpdated = Seafox.latestTime;
        this.destroyer = destroyer;
        this.x = destroyer.m_dX + 35.0;
        this.y = destroyer.m_dY + 8.0;
        this.yDetonate = Player.m_dY + Player.m_dSubHeight / 2.0;
        ++activeCharges;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3, double d4) {
        double d5;
        if (this.status == 4 && d3 > this.x - (d5 = 10.0 + 15.0 * (double)(Seafox.latestTime - this.timeStatusUpdated) / 500.0) && d < this.x + d5 && d4 > this.y - d5 && d2 < this.y + d5) {
            double d6 = 0.0;
            d6 = Math.abs(this.y - d4) < Math.abs(this.y - d2) ? Math.abs(this.y - d4) : Math.abs(this.y - d2);
            double d7 = Math.acos(d6 / d5);
            double d8 = Math.abs(d5 * Math.sin(d7));
            if (d3 >= this.x - d8 && d <= this.x + d8) {
                return true;
            }
        }
        return false;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3) {
        if (this.status == 3) {
            double d4 = 3.0;
            double d5 = Math.abs(this.x + d4 - d);
            double d6 = Math.abs(this.y + d4 - d2);
            double d7 = d3 + d4;
            if (d5 <= d7 && d6 <= d7 && Math.sqrt(d5 * d5 + d6 * d6) <= d7) {
                this.Detonate();
                return true;
            }
        }
        return false;
    }

    static {
        COLOR = new Color(255, 128, 0);
    }

    DepthCharge() {
    }

    public void Update() {
        if (this.status == 0) {
            return;
        }
        if (this.status == 1) {
            double d = 20.0 * (double)(Seafox.latestTime - this.timeStatusUpdated) / 1000.0;
            this.x = this.destroyer.m_dX + d + 35.0;
            if (d >= 25.0) {
                this.status = 2;
                this.timeStatusUpdated = Seafox.latestTime;
                this.x0 = this.x;
                this.y0 = this.y;
            }
            return;
        }
        if (this.status == 2) {
            double d = (double)(Seafox.latestTime - this.timeStatusUpdated) / 1000.0;
            this.x = this.x0 + (20.0 + this.destroyer.m_dVelocity) * d;
            this.y = this.y0 + 15.0 * d;
            if (this.y >= 55.0) {
                this.status = 3;
                this.timeStatusUpdated = Seafox.latestTime;
                this.x0 = this.x;
                this.y0 = this.y;
                this.angle0 = 3.1415926536 * Seafox.random.nextDouble();
                this.tumbleRate = 2.0 * (Seafox.random.nextDouble() - 0.5) * 4.0;
                this.xVelocity = 2.0 * (Seafox.random.nextDouble() - 0.5) * 6.0;
                Seafox.m_bPlaySplash = true;
            }
            return;
        }
        if (this.status == 3) {
            double d = (double)(Seafox.latestTime - this.timeStatusUpdated) / 1000.0;
            this.x = this.x0 + this.xVelocity * d;
            this.y = this.y0 + 20.0 * d;
            this.angle = this.angle0 + this.tumbleRate * d;
            if (this.y >= this.yDetonate) {
                this.Detonate();
            }
            return;
        }
        if (this.status == 4) {
            if (Seafox.latestTime - this.timeStatusUpdated >= 500L) {
                this.status = 0;
                this.timeStatusUpdated = Seafox.latestTime;
                activeCharges += -1;
                return;
            }
            double d = 10.0 + 15.0 * (double)(Seafox.latestTime - this.timeStatusUpdated) / 500.0;
            int n = 0;
            do {
                Seafox.m_magneticMine[n].CollisionAnalysis(this.x, this.y, d);
            } while (++n < 3);
        }
    }

    public void Detonate() {
        this.status = 4;
        this.timeStatusUpdated = Seafox.latestTime;
        Seafox.playBigExplosion = true;
    }

    public boolean Detonate(double d, double d2) {
        double d3;
        if (this.status == 3 && d >= this.x - (d3 = 6.0) && d <= this.x + d3 && d2 >= this.y - d3 && d2 <= this.y + d3) {
            this.Detonate();
            return true;
        }
        return false;
    }

    public boolean Detonate(double d, double d2, double d3, double d4) {
        double d5;
        if (this.status == 4) {
            return this.CollisionAnalysis(d, d2, d3, d4);
        }
        if (this.status == 3 && d3 >= this.x - (d5 = 6.0) && d <= this.x + d5 && d4 >= this.y - d5 && d2 <= this.y + d5) {
            this.Detonate();
            return true;
        }
        return false;
    }
}
