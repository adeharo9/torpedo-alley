package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  Player
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

class MagneticMine {
    public static Image m_imageMine;
    public static double m_dMineRadius;
    public static final int MAX_MINES = 3;
    public static int m_nMineLimit;
    public static int m_nActiveMines;
    static final int MIN_LAUNCH_INTERVAL = 10000;
    static final int MAX_LAUNCH_INTERVAL = 15000;
    static final double VELOCITY = 5.0;
    static final int BLINK_PERIOD = 2000;
    static final double LAUNCH_X = 12.0;
    static final double LAUNCH_Y = 10.0;
    static final double MIN_BLAST_RADIUS = 10.0;
    static final double MAX_BLAST_RADIUS = 25.0;
    public int m_ndx;
    public int m_nStatus;
    public static final int INACTIVE = 0;
    public static final int NORMAL = 1;
    static final int EXPLODING = 2;
    long m_lTimeStatusUpdated;
    long m_lTimeCoordsUpdated;
    double m_dX;
    double m_dY;

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            int n = (int)this.m_dX;
            int n2 = (int)this.m_dY;
            int n3 = (int)m_dMineRadius;
            graphics.drawImage(m_imageMine, n, n2, null);
            double d = (double)((int)(Seafox.latestTime - this.m_lTimeStatusUpdated) % 2000) / 2000.0;
            double d2 = 0.25 + Math.sin(Math.PI * d) * 0.75;
            Color color = new Color(0.5f, (float)d2, (float)(d2 / 2.0));
            graphics.setColor(color);
            graphics.fillRect(n + n3 - 1, n2, 3, 3);
            graphics.fillRect(n, n2 + n3 - 1, 3, 3);
            graphics.fillRect(n + 2 * n3 - 2, n2 + n3 - 1, 3, 3);
            graphics.fillRect(n + n3 - 1, n2 + 2 * n3 - 2, 3, 3);
            return;
        }
        if (this.m_nStatus == 2) {
            Image image = DepthCharge.imageBlast[(int)(Seafox.latestTime - this.m_lTimeStatusUpdated) * 4 / 500];
            int n = (int)(this.m_dX + m_dMineRadius) - image.getWidth(null) / 2;
            int n4 = (int)(this.m_dY + m_dMineRadius) - image.getHeight(null) / 2;
            graphics.drawImage(image, n, n4, null);
        }
    }

    public void Launch(EnemySub enemySub) {
        this.m_nStatus = 1;
        this.m_lTimeStatusUpdated = Seafox.latestTime;
        this.m_lTimeCoordsUpdated = Seafox.latestTime;
        this.m_dX = enemySub.m_dX + 12.0;
        this.m_dY = enemySub.m_dY + 10.0;
        ++m_nActiveMines;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3) {
        if (this.m_nStatus == 1) {
            double d4 = Math.abs(this.m_dX + m_dMineRadius - d);
            double d5 = Math.abs(this.m_dY + m_dMineRadius - d2);
            double d6 = d3 + m_dMineRadius;
            if (d4 <= d6 && d5 <= d6 && Math.sqrt(d4 * d4 + d5 * d5) <= d6) {
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    public boolean CollisionAnalysis(double d, double d2, double d3, double d4) {
        if (this.m_nStatus == 2) {
            double d5 = 10.0 + 15.0 * (double)(Seafox.latestTime - this.m_lTimeStatusUpdated) / 500.0;
            double d6 = this.m_dX + m_dMineRadius;
            double d7 = this.m_dY + m_dMineRadius;
            if (d3 > d6 - d5 && d < d6 + d5 && d4 > d7 - d5 && d2 < d7 + d5) {
                double d8 = 0.0;
                d8 = Math.abs(d7 - d4) < Math.abs(d7 - d2) ? Math.abs(d7 - d4) : Math.abs(d7 - d2);
                double d9 = Math.acos(d8 / d5);
                double d10 = Math.abs(d5 * Math.sin(d9));
                if (d3 >= d6 - d10 && d <= d6 + d10) {
                    return true;
                }
            }
        }
        return false;
    }

    MagneticMine() {
    }

    public void Update() {
        if (this.m_nStatus == 0) {
            return;
        }
        if (this.m_nStatus == 1) {
            double d = 0.0;
            double d2 = Player.m_dY + 10.0;
            double d3 = this.m_dX + m_dMineRadius;
            double d4 = this.m_dY + m_dMineRadius;
            switch (this.m_ndx) {
                case 0: {
                    d = Player.m_dX + 10.0;
                    break;
                }
                case 1: {
                    d = Player.m_dX + 30.0;
                    break;
                }
                case 2: {
                    d = Player.m_dX + 50.0;
                }
            }
            double d5 = 0.0;
            double d6 = 0.0;
            if (d - d3 < 0.0) {
                d5 = -5.0;
            }
            if (d - d3 > 0.0) {
                d5 = 5.0;
            }
            if (d2 - d4 < 0.0) {
                d6 = -5.0;
            }
            if (d2 - d4 > 0.0) {
                d6 = 5.0;
            }
            double d7 = (double)(Seafox.latestTime - this.m_lTimeCoordsUpdated) / 1000.0;
            this.m_dX += d5 * d7;
            this.m_dY += d6 * d7;
            this.m_lTimeCoordsUpdated = Seafox.latestTime;
            return;
        }
        if (this.m_nStatus == 2) {
            if (Seafox.latestTime - this.m_lTimeStatusUpdated >= 500L) {
                this.m_nStatus = 0;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                m_nActiveMines += -1;
                return;
            }
            double d = 10.0 + 15.0 * (double)(Seafox.latestTime - this.m_lTimeStatusUpdated) / 500.0;
            double d8 = this.m_dX + m_dMineRadius;
            double d9 = this.m_dY + m_dMineRadius;
            int n = 0;
            do {
                if (n == this.m_ndx) continue;
                Seafox.m_magneticMine[n].CollisionAnalysis(d8, d9, d);
            } while (++n < 3);
            n = 0;
            do {
                Seafox.m_depthCharge[n].CollisionAnalysis(d8, d9, d);
            } while (++n < 3);
        }
    }

    public boolean Detonate(double d, double d2) {
        if (this.m_nStatus == 1) {
            double d3 = this.m_dX + m_dMineRadius;
            double d4 = this.m_dY + m_dMineRadius;
            double d5 = Math.abs(d3 - d);
            double d6 = Math.abs(d4 - d2);
            if (d5 <= m_dMineRadius && d6 <= m_dMineRadius && Math.sqrt(d5 * d5 + d6 * d6) <= m_dMineRadius) {
                this.m_nStatus = 2;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    public boolean Detonate(double d, double d2, double d3, double d4) {
        if (this.m_nStatus == 2) {
            return this.CollisionAnalysis(d, d2, d3, d4);
        }
        if (this.m_nStatus == 1) {
            double d5 = this.m_dX + m_dMineRadius;
            double d6 = this.m_dY + m_dMineRadius;
            if (d3 > d5 - m_dMineRadius && d < d5 + m_dMineRadius && d4 > d6 - m_dMineRadius && d2 < d6 + m_dMineRadius) {
                double d7 = 0.0;
                d7 = Math.abs(d6 - d4) < Math.abs(d6 - d2) ? Math.abs(d6 - d4) : Math.abs(d6 - d2);
                double d8 = Math.acos(d7 / m_dMineRadius);
                double d9 = Math.abs(m_dMineRadius * Math.sin(d8));
                if (d3 >= d5 - d9 && d <= d5 + d9) {
                    this.m_nStatus = 2;
                    this.m_lTimeStatusUpdated = Seafox.latestTime;
                    Seafox.playBigExplosion = true;
                    return true;
                }
            }
        }
        return false;
    }
}
