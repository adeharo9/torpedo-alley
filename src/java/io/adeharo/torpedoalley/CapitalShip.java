package io.adeharo.torpedoalley;

// AWT: Abstract Window Toolkit, the foundation of later-to-be Swing (Java GUI framework)
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Cargo ships that have to be destroyed
 */
class CapitalShip {
    // Static properties, they are the same for all capita ships
    /**
     * Maximum number of capital ships in this game
     */
    public static final int SHIPS = 10;
    /**
     * Number of different capital ship images
     */
    public static final int SHIP_IMAGES = 4;
    /**
     * Number of frames that comprise the animation of the wake of the ship
     */
    public static final int WAKE_IMAGES = 4;
    /**
     * Number of frames that comprise the animation of the blast of the ship
     */
    public static final int BLAST_IMAGES = 4;
    /**
     * Number of frames that comprise the animation of the sinking of this ship
     */
    public static final int SINK_IMAGES = 4;
    /**
     * Duration of the blast animation, in ms
     */
    public static final int BLAST_TIME = 500;
    /**
     * Duration of the sinking animation, in ms
     */
    public static final int SINK_TIME = 999;
    /**
     * Duration of the score showing, in ms
     */
    public static final int SCORE_TIME = 999;
    /**
     * Y coordinate at which the waterline for capital ships is located
     */
    public static final int Y_WATERLINE = 22;
    /**
     * Margin on the horizontal coordinate for actively rendering objects, even when outside of the viewport
     * TODO: confirm???
     */
    public static final int X_MARGIN = 40;
    /**
     * Speed of capital ships
     */
    public static final double VELOCITY = 20.0;
    /**
     * Distance between two different capital ships in the X coordinate
     */
    public static int xSpacing;
    /**
     * Number of ships sunk by the player
     */
    public static int shipsSunk;
    /**
     * Timestamp at which the game stated
     */
    public static long startTime;
    /**
     * X coordinate of the origin used for capital ships spawn calculations
     * TODO: ???
     */
    public static double xOriginStart;
    /**
     * TODO: ??? Don't really know, but seems to be moving with capital ship speed, yet it's static
     */
    public static double xOriginNow;
    /**
     * Width of the wake animation
     */
    public static int wakeWidth;
    /**
     * Height of the wake animation
     */
    public static int wakeHeight;
    /**
     * Images of the capital ships
     */
    public static Image[] imageShip = new Image[SHIP_IMAGES];
    /**
     * Frames of the wake animation
     */
    public static Image[] imageWake = new Image[WAKE_IMAGES];
    /**
     * Frames of the blast animation
     */
    public static Image[] imageBlast = new Image[BLAST_IMAGES];
    /**
     * Frames of the sinking animation
     */
    public static Image[] imageSink = new Image[SINK_IMAGES];
    /**
     * Normal status, i.e. the ship is floating and moving
     */
    static final int NORMAL = 0;
    /**
     * Sinking status, i.e. the ship was hit
     */
    static final int SINKING = 1;
    /**
     * Sunk status, i.e. the ship is no longer active
     */
    static final int SUNK = 2;
    // Instance properties
    /**
     * Whether this ship is sinkable or not
     */
    public boolean sinkable;
    /**
     * Offset to apply to `this` capital ship's x-origin in the x coordinate with respect to the playfield's x-origin
     */
    double xOffset;
    /**
     * Current ship's image index from the array of ship images
     */
    int dxImage;
    int status;
    int points;
    int xCenter;
    int x;
    int y;
    /**
     * Timestamp at which `this` ship was hit by a torpedo
     */
    long timeHit;

    /**
     * Default constructor
     */
    CapitalShip() {
    }

    /**
     * Draw `this` capital ship instance.
     *
     * @param graphics
     */
    public void Draw(Graphics graphics) {
        Image image;
        int n;
        int n2;
        // Do not allow this ship to sink while drawing it
        // TODO: why though? Because of multithreading?
        sinkable = false;

        // If the ship is already sunk, do nothing
        if (status == SUNK) {
            return;
        }

        // If the ship is in status normal
        if (status == NORMAL) {
            // Absolute position of `this` capital ship's x-origin with respect to the capital ship's spawning origin
            double d = xOffset + xOriginNow;
            // TODO: why 0.5?
            xCenter = (int)(d + 0.5);
            if (xCenter > 0) {
                // TODO: I think this modulo ensures coordinates are always within bounds of maximum 10 ships?
                xCenter %= xSpacing * SHIPS;
            }

            // If the ship's center is past the right side of the playfield + extra margin (i.e. outside of the screen
            // on the right), reset its position to the origin where capital ships spawn
            if (xCenter > Seafox.playfieldWidth + X_MARGIN) {
                xCenter -= xSpacing * SHIPS;
            }

            // If the ship's center is past the left side of the playfield + extra margin, (i.e. outside of the screen)
            // then there is nothing to draw, so return
            if (xCenter < -X_MARGIN) {
                return;
            }
        }

        // Compute the elapsed time since the ship was last hit, in ms
        int elapsedTimeSinceHit = (int)(Seafox.latestTime - timeHit);

        // If the ship is sinking and all animations have finished (blast, sink and score), then mark it as sunk
        if (status == SINKING && elapsedTimeSinceHit > (BLAST_TIME + SINK_TIME + SCORE_TIME)) {
            status = SUNK;
            return;
        }

        // If the ship is in normal status or sinking but the blast animation has not yet finished
        if (status == NORMAL || status == SINKING && elapsedTimeSinceHit < BLAST_TIME) {
            // Get this ship's image
            Image image2 = imageShip[dxImage];
            // X coordinate for starting the ship's rendering (left side of the image)
            x = xCenter - image2.getWidth(null) / 2;
            // Y coordinate for starting the ship's rendeding (upper side of the image)
            y = Y_WATERLINE - image2.getHeight(null);
            // If the status is normal
            if (status == NORMAL) {
                // X coordinate for starting the wake's rendering (left side of the image)
                //
                // "+5" because all ships have the rear part such that the "touching point" with the water is 5 pixels
                // into the image, so we have to overlap it those 5 pixels to make it look realistic
                n2 = x - wakeWidth + 5;
                // Y coordinate for starting the wake's rendering (upper side of the image)
                //
                // +1 because capital ship's wakes are 2 pixels high and this way the wake is centered with respect to
                // the ship's touching point with the water
                n = Y_WATERLINE - wakeHeight + 1;
                // Pick a random wake frame
                image = imageWake[(Seafox.random.nextInt() & Integer.MAX_VALUE) % WAKE_IMAGES];
                // Draw the wake
                graphics.drawImage(image, n2, n, null);
            }
            // Draw the ship
            graphics.drawImage(image2, x, y, null);
        }

        // If at this point the status is still normal, then the ship is sinkable
        if (status == NORMAL) {
            sinkable = true;
            // Every action regarding normal ships should have been drawn already, so return
            return;
        }

        // If the blast animation is still going
        if (elapsedTimeSinceHit <= BLAST_TIME) {
            // X coordinate for starting the blast's rendering (left side of the image)
            // TODO: why -20?
            n2 = xCenter - 20;
            // Y coordinate for starting the blast's rendering (upper side of the image)
            // TODO: is it 0 because the waterline for capital ship's is already too low?
            n = 0;
            // Take a random blast frame
            image = imageBlast[(Seafox.random.nextInt() & Integer.MAX_VALUE) % 4];
            // Draw the blast
            graphics.drawImage(image, n2, n, null);
            // Every animation regarding blasted ships (but not yet sinking) should have been drawn already, so return
            return;
        }

        // If the ship is in the middle of the sinking animation
        if (elapsedTimeSinceHit <= BLAST_TIME + SINK_TIME) {
            // Calculate the index of the frame for the sinking animation
            // TODO: why 249? Is it supposed to be 250? And given that SINK_TIME is ~1000 (and that there are 4 frames
            //       in the sinking animation), is it supposed to tell which frame it is that goes now?
            n2 = (elapsedTimeSinceHit - BLAST_TIME) / 249;
            // Get the frame of the sinking animation
            Image image3 = imageSink[n2];
            // X coordinate for rendering the sinking frame (same as the original ship image, so they have coinciding
            // top-left corners)
            int n4 = xCenter - image3.getWidth(null) / 2;
            // Y coordinate for rendering the sinking frame (same as the original ship image, so they have coinciding
            // top-left corners)
            int n5 = Y_WATERLINE - image3.getHeight(null);
            // Draw the frame
            graphics.drawImage(image3, n4, n5, null);
            // Every animation regarding sinking ships should have been drawn already, so return
            return;
        }
        // Draw the amount of points given by the ship
        // Set the font
        graphics.setFont(Seafox.fontScorePopup);
        // Set the color
        graphics.setColor(Color.yellow);
        // Create the string
        String string = "" + points;
        // Calculate its x position
        // TODO: why 20? Is it just a margin?
        n = xCenter - 20;
        // If the number has 4 digits, it needs a bit of correction to stay within bounds of the ship's image
        if (points >= 1000) {
            n -= 7;
        }
        // Draw the score
        graphics.drawString(string, n, Y_WATERLINE);
    }

    /**
     * @param d x coordinate of the point to check for collision
     * @param d2 y coordinate of the point to check for collision
     * @return Whether ([d], [d2]) collides with this capital ship
     */
    public boolean CollisionAnalysis(double d, double d2) {
        // Variable for half the width of the bounding box of the capital ship
        int n;
        // Check that the input coordinates are within the bounding box of the capital ship
        // TODO: seems the bounding box measures 10px in height (starting from the waterline) and <image width>+10px
        //       (5px on each side of the image)
        if (sinkable && d2 <= 22.0 && d2 >= 12.0 && (int)d >= xCenter - (n = imageShip[dxImage].getWidth(null) / 2 - 5) && (int)d <= xCenter + n) {
            // If the ship was hit, the new status is sinking
            status = SINKING;
            // Record the hit time
            timeHit = Seafox.latestTime;
            // Compute the number of points this ship gives, according to the current mission and how many ships have
            // already been sunk
            points = (Seafox.mission - 1) * 1000 + ++shipsSunk * 100;
            // Add them to the current game's score
            Seafox.score += points;
            // Play a big explosion sound
            // TODO: is this for the sound?
            Seafox.playBigExplosion = true;
            // The game ends when all ships have been destroyed
            if (shipsSunk == SHIPS) {
                Seafox.player.MissionComplete();
            }
            // Every side action relating to collisions is taken, so return
            return true;
        }
        // No collision detected, nothing to do here
        return false;
    }
}
