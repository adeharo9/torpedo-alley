package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Graphics;
import java.awt.Image;

class SupplySub {
    public static Image m_imageSub;
    public static double m_dImageWidth;
    public static double m_dImageHeight;
    public static double m_dY;
    public double m_dX;
    public static double m_dXrelease;
    double m_dXlaunch;
    static final double VELOCITY = 25.0;
    static final int FREQUENCY = 55000;
    static final int BLAST_TIME = 500;
    public long m_lLaunchTime;
    long m_lTimeHit;
    boolean m_bReleased;
    public int m_nStatus;
    public static final int INACTIVE = 0;
    public static final int NORMAL = 1;
    static final int EXPLODING = 2;
    static final double CONNING_TOWER_LEFT = 19.0;
    static final double CONNING_TOWER_RIGHT = 31.0;
    static final double CONNING_TOWER_TOP = 0.0;
    static final double CONNING_TOWER_BOTTOM = 8.0;
    static final double HULL_LEFT = 1.0;
    static final double HULL_RIGHT = 57.0;
    static final double HULL_TOP = 8.0;
    static final double HULL_BOTTOM = 20.0;

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 0) {
            return;
        }
        int n = (int)(this.m_dX + 0.5);
        int n2 = (int)(m_dY + 0.5);
        graphics.drawImage(m_imageSub, n, n2, null);
        if (this.m_nStatus == 2) {
            Image image = CapitalShip.imageBlast[(Seafox.random.nextInt() & Integer.MAX_VALUE) % 4];
            int n3 = (int)(this.m_dX + (m_dImageWidth - (double)image.getWidth(null)) / 2.0);
            int n4 = (int)(m_dY + (m_dImageHeight - (double)image.getHeight(null)) / 2.0);
            graphics.drawImage(image, n3, n4, null);
        }
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (this.m_nStatus == 1 && d >= this.m_dX && d <= this.m_dX + m_dImageWidth && d2 >= m_dY && d2 <= m_dY + m_dImageHeight) {
            double d3 = d - this.m_dX;
            double d4 = d2 - m_dY;
            if (d3 >= 19.0 && d3 <= 31.0 && d4 >= 0.0 && d4 <= 8.0 || d3 >= 1.0 && d3 <= 57.0 && d4 >= 8.0 && d4 <= 20.0) {
                this.m_nStatus = 2;
                this.m_lTimeHit = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    SupplySub() {
    }

    public void Update() {
        if (this.m_nStatus == 0) {
            if ((int)(Seafox.latestTime - this.m_lLaunchTime) >= 55000) {
                this.m_nStatus = 1;
                this.m_lLaunchTime = Seafox.latestTime;
                this.m_dX = this.m_dXlaunch = -m_dImageWidth;
                this.m_bReleased = false;
            }
            return;
        }
        if (this.m_nStatus == 2) {
            if ((int)(Seafox.latestTime - this.m_lTimeHit) >= 500) {
                this.m_nStatus = 0;
            }
            return;
        }
        this.m_dX = this.m_dXlaunch + 25.0 * (double)(Seafox.latestTime - this.m_lLaunchTime) / 1000.0;
        if (this.m_dX > (double)Seafox.playfieldWidth) {
            this.m_nStatus = 0;
            return;
        }
        if (this.m_dX >= m_dXrelease && !this.m_bReleased) {
            this.m_bReleased = true;
            Seafox.m_dolphin.Release(this);
        }
        int n = 0;
        do {
            if (!Seafox.m_depthCharge[n].Detonate(this.m_dX + 19.0, m_dY + 0.0, this.m_dX + 31.0, m_dY + 8.0) && !Seafox.m_depthCharge[n].Detonate(this.m_dX + 1.0, m_dY + 8.0, this.m_dX + 57.0, m_dY + 20.0)) continue;
            this.m_nStatus = 2;
            this.m_lTimeHit = Seafox.latestTime;
            Seafox.playBigExplosion = true;
            return;
        } while (++n < 3);
        n = 0;
        do {
            if (!Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX + 19.0, m_dY + 0.0, this.m_dX + 31.0, m_dY + 8.0) && !Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX + 1.0, m_dY + 8.0, this.m_dX + 57.0, m_dY + 20.0)) continue;
            this.m_nStatus = 2;
            this.m_lTimeHit = Seafox.latestTime;
            Seafox.playBigExplosion = true;
            return;
        } while (++n < 3);
    }
}
