package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  CapitalShip
 *  DepthCharge
 *  Destroyer
 *  Dolphin
 *  EnemySub
 *  EnemyTorpedo
 *  HorizontalTorpedo
 *  HospitalShip
 *  KillerFish
 *  MagneticMine
 *  Package
 *  Player
 *  SupplySub
 *  VerticalTorpedo
 */
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.util.Random;

public class Seafox
extends Applet
implements Runnable {
    int[] m_nKeyBuffer = new int[4];
    Thread m_threadAnim;
    boolean m_bInitialized;
    static Random random = new Random();
    public static int m_nMouseX;
    public static int m_nMouseY;
    public static boolean m_bMouseInWindow;
    public static boolean m_bButtonDown;
    public static boolean m_bSpacebarPressed;
    final int m_nHistoryFrames = 10;
    public static int m_nFrameNumber;
    long[] m_lTime = new long[10];
    /**
     * Timestamp at which the current frame started computing
     */
    public static long latestTime;
    public static double m_dSecondsPerFrame;
    MediaTracker m_mediaTracker = new MediaTracker(this);
    Image m_imageLoading;
    public static int m_nWindowWidth;
    public static int m_nWindowHeight;
    public static int playfieldWidth;
    public static int m_nPlayfieldHeight;
    public static Font fontScorePopup;
    public static final int SURFACE_HEIGHT = 60;
    public static final Color SURFACE_COLOR;
    public static final Color UNDERWATER_COLOR;
    Image m_bufferPlayfield;
    Image m_bufferSurface;
    Image m_imageWater;
    boolean m_bSoundEffectsOn = true;
    AudioClip m_acBigExplosion;
    AudioClip m_acLittleExplosion;
    AudioClip m_acFireTorpedo;
    AudioClip m_acOutOfTorpedoes;
    AudioClip m_acClank;
    AudioClip m_acSupplies;
    AudioClip m_acShark;
    AudioClip m_acSplash;
    public static boolean playBigExplosion;
    public static boolean m_bPlayLittleExplosion;
    public static boolean m_bPlayFireTorpedo;
    public static boolean m_bPlayOutOfTorpedoes;
    public static boolean m_bPlayClank;
    public static boolean m_bPlaySupplies;
    public static boolean m_bPlayShark;
    public static boolean m_bPlaySplash;
    public static final int SCOREBAR_HEIGHT = 20;
    static final int SCOREBAR_BASELINE = 16;
    static final int SCOREBAR_FONT_SIZE = 14;
    static final int SCOREBAR_X_MARGIN = 4;
    static final int SCOREBAR_ZONES = 5;
    static final int SCOREBAR_MISSION_ZONE = 0;
    static final int SCOREBAR_SHIPS_SUNK_ZONE = 1;
    static final int SCOREBAR_SCORE_ZONE = 2;
    static final int SCOREBAR_TORPEDO_ZONE = 3;
    static final int SCOREBAR_FUEL_ZONE = 4;
    static final Color SCOREBAR_BACKGROUND_COLOR;
    static final Color SCOREBAR_CAPTION_COLOR;
    static final Color SCOREBAR_VALUE_COLOR;
    Image m_bufferScorebar;
    Font m_fontScorebar;
    int[] m_nScorebarZoneCaptionWidth = new int[5];
    int[] m_nScorebarZoneDataX = new int[5];
    int[] m_nScorebarZoneDataWidth = new int[5];
    int[] m_nScorebarZoneValue = new int[5];
    public static Player player;
    public static VerticalTorpedo m_verticalTorpedo;
    public static HorizontalTorpedo m_horizontalTorpedo;
    public static CapitalShip[] m_capitalShip;
    public static HospitalShip[] m_hospitalShip;
    public static Destroyer[] m_destroyer;
    public static DepthCharge[] m_depthCharge;
    public static EnemySub[] m_enemySub;
    public static EnemyTorpedo[] m_enemyTorpedo;
    public static MagneticMine[] m_magneticMine;
    public static SupplySub m_supplySub;
    public static Dolphin m_dolphin;
    public static Package m_package;
    public static KillerFish m_killerFish;
    public static int score;
    public static int mission;
    public static int m_nSubsLeft;
    public static int m_nTimesPlayed;
    public static boolean m_bMissionComplete;
    public static boolean m_bDisplayMissionTitle;
    public static boolean m_bGameOver;
    Font m_fontMessage = new Font("Helvetica", 1, 20);
    Color m_colorMessage = Color.white;
    String m_stringStart = new String("Click Here to Start Game");
    int m_nXstartString;
    int m_nYstartString;
    String m_stringOutOfFuel = new String("Out of Fuel");
    int m_nXoutOfFuel;
    int m_nYoutOfFuel;
    String m_stringGameOver = new String("Game Over");
    int m_nXgameOver;
    int m_nYgameOver;
    String m_stringMissionTitle = new String("Mission 0");
    int m_nXmissionTitle;
    int m_nYmissionTitle;
    String m_stringMissionComplete = new String("Mission Complete");
    int m_nXmissionComplete;
    int m_nYmissionComplete;
    String[] m_stringSubsLeft = new String[3];
    int[] m_nXsubsLeft = new int[3];
    int m_nYsubsLeft;

    private void MissionInit() {
        CapitalShip.shipsSunk = 0;
        int n = 0;
        do {
            Seafox.m_capitalShip[n].dxImage = (random.nextInt() & Integer.MAX_VALUE) % 4;
            Seafox.m_capitalShip[n].status = CapitalShip.NORMAL;
            Seafox.m_capitalShip[n].sinkable = false;
            Seafox.m_capitalShip[n].xOffset = ((double)n + 0.7 * (random.nextDouble() - 0.5)) * (double)CapitalShip.xSpacing;
        } while (++n < CapitalShip.SHIPS);
        n = 0;
        do {
            Seafox.m_hospitalShip[n].m_dXoffset = ((double)n + 0.7 * (random.nextDouble() - 0.5)) * (double)HospitalShip.m_nXspacing;
        } while (++n < 3);
        switch (mission) {
            case 1: {
                Destroyer.m_nShipLimit = 0;
                DepthCharge.chargeLimit = 0;
                EnemySub.m_nSubLimit = 0;
                EnemyTorpedo.m_nTorpedoLimit = 0;
                MagneticMine.m_nMineLimit = 0;
                break;
            }
            case 2: {
                Destroyer.m_nShipLimit = 3;
                DepthCharge.chargeLimit = 3;
                EnemySub.m_nSubLimit = 0;
                EnemyTorpedo.m_nTorpedoLimit = 0;
                MagneticMine.m_nMineLimit = 0;
                break;
            }
            case 3: {
                Destroyer.m_nShipLimit = 3;
                DepthCharge.chargeLimit = 3;
                EnemySub.m_nSubLimit = 3;
                EnemyTorpedo.m_nTorpedoLimit = 0;
                MagneticMine.m_nMineLimit = 0;
                break;
            }
            case 4: {
                Destroyer.m_nShipLimit = 3;
                DepthCharge.chargeLimit = 3;
                EnemySub.m_nSubLimit = 3;
                EnemyTorpedo.m_nTorpedoLimit = 3;
                MagneticMine.m_nMineLimit = 0;
                break;
            }
            case 5: {
                Destroyer.m_nShipLimit = 3;
                DepthCharge.chargeLimit = 3;
                EnemySub.m_nSubLimit = 3;
                EnemyTorpedo.m_nTorpedoLimit = 3;
                MagneticMine.m_nMineLimit = 3;
            }
        }
        this.m_stringMissionTitle = new String("Mission " + mission);
        m_bDisplayMissionTitle = true;
    }

    public void start() {
        if (this.m_threadAnim == null) {
            this.m_threadAnim = new Thread(this);
            this.m_threadAnim.start();
        }
    }

    public void stop() {
        if (this.m_threadAnim != null) {
            this.m_threadAnim.stop();
            this.m_threadAnim = null;
        }
    }

    public boolean keyUp(Event event, int n) {
        if (n == 32) {
            m_bSpacebarPressed = false;
        }
        return true;
    }

    public boolean mouseExit(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bMouseInWindow = false;
        m_bButtonDown = false;
        return true;
    }

    public boolean mouseMove(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bMouseInWindow = true;
        m_bButtonDown = false;
        return true;
    }

    public boolean mouseDown(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bButtonDown = true;
        return true;
    }

    public boolean keyDown(Event event, int n) {
        if (n == 32) {
            m_bSpacebarPressed = true;
        }
        if (n == 113 || n == 81) {
            this.m_bSoundEffectsOn = false;
        }
        if (n == 115 || n == 83) {
            this.m_bSoundEffectsOn = true;
        }
        return true;
    }

    static {
        fontScorePopup = new Font("Helvetica", 1, 24);
        SURFACE_COLOR = new Color(0, 0, 128);
        UNDERWATER_COLOR = new Color(0, 0, 64);
        SCOREBAR_BACKGROUND_COLOR = new Color(0, 43, 128);
        SCOREBAR_CAPTION_COLOR = new Color(48, 128, 255);
        SCOREBAR_VALUE_COLOR = new Color(149, 184, 255);
        player = new Player();
        m_verticalTorpedo = new VerticalTorpedo();
        m_horizontalTorpedo = new HorizontalTorpedo();
        m_capitalShip = new CapitalShip[10];
        m_hospitalShip = new HospitalShip[3];
        m_destroyer = new Destroyer[3];
        m_depthCharge = new DepthCharge[3];
        m_enemySub = new EnemySub[3];
        m_enemyTorpedo = new EnemyTorpedo[3];
        m_magneticMine = new MagneticMine[3];
        m_supplySub = new SupplySub();
        m_dolphin = new Dolphin();
        m_package = new Package();
        m_killerFish = new KillerFish();
        mission = 1;
        m_nSubsLeft = 3;
    }

    public boolean mouseEnter(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bMouseInWindow = true;
        return true;
    }

    public boolean mouseDrag(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bMouseInWindow = true;
        return true;
    }

    private boolean UpdateScorebar() {
        Graphics graphics = this.m_bufferScorebar.getGraphics();
        graphics.setFont(this.m_fontScorebar);
        boolean bl = false;
        if (mission != this.m_nScorebarZoneValue[0]) {
            bl = true;
            graphics.setColor(SCOREBAR_BACKGROUND_COLOR);
            graphics.fillRect(this.m_nScorebarZoneDataX[0], 0, this.m_nScorebarZoneDataWidth[0], 20);
            graphics.setColor(SCOREBAR_VALUE_COLOR);
            graphics.drawString(new String("" + mission), this.m_nScorebarZoneDataX[0], 16);
            this.m_nScorebarZoneValue[0] = mission;
        }
        if (CapitalShip.shipsSunk != this.m_nScorebarZoneValue[1]) {
            bl = true;
            graphics.setColor(SCOREBAR_BACKGROUND_COLOR);
            graphics.fillRect(this.m_nScorebarZoneDataX[1], 0, this.m_nScorebarZoneDataWidth[1], 20);
            graphics.setColor(SCOREBAR_VALUE_COLOR);
            graphics.drawString(new String("" + CapitalShip.shipsSunk), this.m_nScorebarZoneDataX[1], 16);
            this.m_nScorebarZoneValue[1] = CapitalShip.shipsSunk;
        }
        if (score != this.m_nScorebarZoneValue[2]) {
            bl = true;
            graphics.setColor(SCOREBAR_BACKGROUND_COLOR);
            graphics.fillRect(this.m_nScorebarZoneDataX[2], 0, this.m_nScorebarZoneDataWidth[2], 20);
            graphics.setColor(SCOREBAR_VALUE_COLOR);
            graphics.drawString(new String("" + score), this.m_nScorebarZoneDataX[2], 16);
            this.m_nScorebarZoneValue[2] = score;
        }
        if (Player.m_nTorpedoes != this.m_nScorebarZoneValue[3]) {
            bl = true;
            graphics.setColor(SCOREBAR_BACKGROUND_COLOR);
            graphics.fillRect(this.m_nScorebarZoneDataX[3], 0, this.m_nScorebarZoneDataWidth[3], 20);
            graphics.setColor(SCOREBAR_VALUE_COLOR);
            graphics.drawString(new String("" + Player.m_nTorpedoes), this.m_nScorebarZoneDataX[3], 16);
            this.m_nScorebarZoneValue[3] = Player.m_nTorpedoes;
        }
        if ((int)(Player.m_dFuel + 0.5) != this.m_nScorebarZoneValue[4]) {
            bl = true;
            graphics.setColor(SCOREBAR_BACKGROUND_COLOR);
            graphics.fillRect(this.m_nScorebarZoneDataX[4], 0, this.m_nScorebarZoneDataWidth[4], 20);
            graphics.setColor(SCOREBAR_VALUE_COLOR);
            graphics.drawString(new String("" + (int)(Player.m_dFuel + 0.5) + "%"), this.m_nScorebarZoneDataX[4], 16);
            this.m_nScorebarZoneValue[4] = (int)(Player.m_dFuel + 0.5);
        }
        return bl;
    }

    private void StartNewGame() {
        m_bGameOver = false;
        m_bButtonDown = false;
        playBigExplosion = false;
        m_bPlayLittleExplosion = false;
        m_bPlayFireTorpedo = false;
        m_bPlayOutOfTorpedoes = false;
        m_bPlayClank = false;
        m_bPlaySupplies = false;
        m_bPlaySplash = false;
        m_bPlayShark = false;
        mission = 1;
        m_nSubsLeft = 3;
        score = 0;
        ++m_nTimesPlayed;
        this.MissionInit();
        this.SubInit();
    }

    public boolean mouseUp(Event event, int n, int n2) {
        m_nMouseX = n;
        m_nMouseY = n2;
        m_bButtonDown = false;
        return true;
    }

    public void run() {
        if (!this.m_bInitialized) {
            this.Initialization();
        }
        while (true) {
            if (m_bGameOver && m_bButtonDown) {
                this.StartNewGame();
            }
            this.UpdateGameObjects();
            this.DrawPlayfield();
            boolean bl = this.UpdateScorebar();
            if (this.m_bSoundEffectsOn && !m_bGameOver) {
                this.PlaySoundEffects();
            }
            Graphics graphics = this.getGraphics();
            graphics.drawImage(this.m_bufferPlayfield, 0, 0, null);
            if (bl) {
                graphics.drawImage(this.m_bufferScorebar, 0, m_nPlayfieldHeight, null);
            }
            graphics.dispose();
        }
    }

    private void DrawProgressBar(int n, int n2, int n3, int n4, double d) {
        Graphics graphics = this.m_imageLoading.getGraphics();
        graphics.setColor(Color.blue);
        graphics.fillRect(n, n2, (int)((double)n3 * d), n4);
        graphics.dispose();
        Graphics graphics2 = this.getGraphics();
        graphics2.drawImage(this.m_imageLoading, 0, 0, null);
        graphics2.dispose();
    }

    private void DrawPlayfield() {
        Graphics graphics = this.m_bufferPlayfield.getGraphics();
        graphics.drawImage(this.m_bufferSurface, 0, 0, null);
        graphics.setColor(UNDERWATER_COLOR);
        graphics.fillRect(0, 60, playfieldWidth, m_nPlayfieldHeight - 60);
        m_verticalTorpedo.Draw(graphics);
        m_horizontalTorpedo.Draw(graphics);
        player.Draw(graphics);
        m_killerFish.Draw(graphics);
        int n = 0;
        do {
            m_capitalShip[n].Draw(graphics);
        } while (++n < 10);
        n = 0;
        do {
            m_hospitalShip[n].Draw(graphics);
        } while (++n < 3);
        n = 0;
        do {
            m_destroyer[n].Draw(graphics);
        } while (++n < 3);
        n = 0;
        do {
            m_depthCharge[n].Draw(graphics);
        } while (++n < 3);
        n = 0;
        do {
            m_enemySub[n].Draw(graphics);
        } while (++n < 3);
        n = 0;
        do {
            m_enemyTorpedo[n].Draw(graphics);
        } while (++n < 3);
        n = 0;
        do {
            m_magneticMine[n].Draw(graphics);
        } while (++n < 3);
        m_dolphin.Draw(graphics);
        m_package.Draw(graphics);
        m_supplySub.Draw(graphics);
        if (Player.m_nStatus == 4) {
            graphics.setFont(this.m_fontMessage);
            graphics.setColor(this.m_colorMessage);
            graphics.drawString(this.m_stringSubsLeft[m_nSubsLeft - 1], this.m_nXsubsLeft[m_nSubsLeft - 1], this.m_nYsubsLeft);
        }
        if (m_bDisplayMissionTitle) {
            graphics.setFont(this.m_fontMessage);
            graphics.setColor(this.m_colorMessage);
            graphics.drawString(this.m_stringMissionTitle, this.m_nXmissionTitle, this.m_nYmissionTitle);
        }
        if (Player.m_nStatus == 3 && !m_bGameOver) {
            graphics.setFont(this.m_fontMessage);
            graphics.setColor(this.m_colorMessage);
            graphics.drawString(this.m_stringMissionComplete, this.m_nXmissionComplete, this.m_nYmissionComplete);
        }
        if (Player.m_nStatus == 0 && Player.m_dFuel <= 0.0) {
            graphics.setFont(this.m_fontMessage);
            graphics.setColor(this.m_colorMessage);
            graphics.drawString(this.m_stringOutOfFuel, this.m_nXoutOfFuel, this.m_nYoutOfFuel);
        }
        if (m_bGameOver) {
            graphics.setFont(this.m_fontMessage);
            graphics.setColor(this.m_colorMessage);
            if (m_nTimesPlayed > 0) {
                graphics.drawString(this.m_stringGameOver, this.m_nXgameOver, this.m_nYgameOver);
            }
            graphics.drawString(this.m_stringStart, this.m_nXstartString, this.m_nYstartString);
        }
    }

    private void UpdateGameObjects() {
        int n;
        latestTime = System.currentTimeMillis();
        int n2 = m_nFrameNumber % 10;
        if (m_nFrameNumber > 10) {
            n = (int)(latestTime - this.m_lTime[n2]);
            m_dSecondsPerFrame = (double)n / 10.0 / 1000.0;
        } else {
            m_dSecondsPerFrame = 0.1;
        }
        this.m_lTime[n2] = latestTime;
        ++m_nFrameNumber;
        if (Seafox.player.m_lRestartTime != 0L && Seafox.player.m_lRestartTime < latestTime) {
            if (m_nSubsLeft > 0) {
                if (mission >= 5 && m_bMissionComplete) {
                    m_bMissionComplete = false;
                    m_bGameOver = true;
                    m_nSubsLeft = 0;
                } else {
                    if (m_bMissionComplete) {
                        ++mission;
                        this.MissionInit();
                        m_bMissionComplete = false;
                    }
                    this.SubInit();
                }
            } else {
                m_bGameOver = true;
                m_bMissionComplete = false;
            }
        }
        player.Update();
        m_killerFish.Update();
        CapitalShip.xOriginNow = CapitalShip.xOriginStart + CapitalShip.VELOCITY * (double)(latestTime - CapitalShip.startTime) / 1000.0;
        HospitalShip.m_dXoriginNow = HospitalShip.m_dXoriginStart + 24.0 * (double)(latestTime - HospitalShip.m_lStartTime) / 1000.0;
        n = 0;
        do {
            m_destroyer[n].Update();
        } while (++n < 3);
        n = 0;
        do {
            m_depthCharge[n].Update();
        } while (++n < 3);
        n = 0;
        do {
            m_enemySub[n].Update();
        } while (++n < 3);
        n = 0;
        do {
            m_enemyTorpedo[n].Update();
        } while (++n < 3);
        n = 0;
        do {
            m_magneticMine[n].Update();
        } while (++n < 3);
        m_supplySub.Update();
        m_dolphin.Update();
        m_package.Update();
        m_verticalTorpedo.Update();
        m_horizontalTorpedo.Update();
    }

    public void paint(Graphics graphics) {
        if (!this.m_bInitialized) {
            return;
        }
        graphics.drawImage(this.m_bufferScorebar, 0, m_nPlayfieldHeight, null);
    }

    private void Initialization() {
        Object object;
        int n;
        Player.m_imageSub = this.getImage(this.getDocumentBase(), "plr_sub.gif");
        this.m_mediaTracker.addImage(Player.m_imageSub, 0);
        CapitalShip.imageShip[0] = this.getImage(this.getDocumentBase(), "capship0.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageShip[0], 1);
        CapitalShip.imageShip[1] = this.getImage(this.getDocumentBase(), "capship1.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageShip[1], 2);
        CapitalShip.imageShip[2] = this.getImage(this.getDocumentBase(), "capship2.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageShip[2], 3);
        CapitalShip.imageShip[3] = this.getImage(this.getDocumentBase(), "capship3.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageShip[3], 4);
        CapitalShip.imageWake[0] = this.getImage(this.getDocumentBase(), "lt_wake0.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageWake[0], 5);
        CapitalShip.imageWake[1] = this.getImage(this.getDocumentBase(), "lt_wake1.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageWake[1], 6);
        CapitalShip.imageWake[2] = this.getImage(this.getDocumentBase(), "lt_wake2.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageWake[2], 7);
        CapitalShip.imageWake[3] = this.getImage(this.getDocumentBase(), "lt_wake3.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageWake[3], 8);
        HospitalShip.m_imageShip = this.getImage(this.getDocumentBase(), "hospship.gif");
        this.m_mediaTracker.addImage(HospitalShip.m_imageShip, 9);
        VerticalTorpedo.m_imageUp = this.getImage(this.getDocumentBase(), "utorpedo.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageUp, 10);
        VerticalTorpedo.m_imageDown = this.getImage(this.getDocumentBase(), "dtorpedo.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageDown, 11);
        VerticalTorpedo.m_imageUpWake[0] = this.getImage(this.getDocumentBase(), "up_wake0.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageUpWake[0], 12);
        VerticalTorpedo.m_imageUpWake[1] = this.getImage(this.getDocumentBase(), "up_wake1.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageUpWake[1], 13);
        VerticalTorpedo.m_imageUpWake[2] = this.getImage(this.getDocumentBase(), "up_wake2.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageUpWake[2], 14);
        VerticalTorpedo.m_imageUpWake[3] = this.getImage(this.getDocumentBase(), "up_wake3.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageUpWake[3], 15);
        VerticalTorpedo.m_imageDownWake[0] = this.getImage(this.getDocumentBase(), "dn_wake0.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageDownWake[0], 16);
        VerticalTorpedo.m_imageDownWake[1] = this.getImage(this.getDocumentBase(), "dn_wake1.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageDownWake[1], 17);
        VerticalTorpedo.m_imageDownWake[2] = this.getImage(this.getDocumentBase(), "dn_wake2.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageDownWake[2], 18);
        VerticalTorpedo.m_imageDownWake[3] = this.getImage(this.getDocumentBase(), "dn_wake3.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageDownWake[3], 19);
        CapitalShip.imageBlast[0] = this.getImage(this.getDocumentBase(), "csblast0.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageBlast[0], 20);
        CapitalShip.imageBlast[1] = this.getImage(this.getDocumentBase(), "csblast1.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageBlast[1], 21);
        CapitalShip.imageBlast[2] = this.getImage(this.getDocumentBase(), "csblast2.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageBlast[2], 22);
        CapitalShip.imageBlast[3] = this.getImage(this.getDocumentBase(), "csblast3.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageBlast[3], 23);
        CapitalShip.imageSink[0] = this.getImage(this.getDocumentBase(), "cs_sink0.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageSink[0], 24);
        CapitalShip.imageSink[1] = this.getImage(this.getDocumentBase(), "cs_sink1.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageSink[1], 25);
        CapitalShip.imageSink[2] = this.getImage(this.getDocumentBase(), "cs_sink2.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageSink[2], 26);
        CapitalShip.imageSink[3] = this.getImage(this.getDocumentBase(), "cs_sink3.gif");
        this.m_mediaTracker.addImage(CapitalShip.imageSink[3], 27);
        SupplySub.m_imageSub = this.getImage(this.getDocumentBase(), "sply_sub.gif");
        this.m_mediaTracker.addImage(SupplySub.m_imageSub, 28);
        Dolphin.m_imageDolphin[0] = this.getImage(this.getDocumentBase(), "dolphin0.gif");
        this.m_mediaTracker.addImage(Dolphin.m_imageDolphin[0], 29);
        Dolphin.m_imageDolphin[1] = this.getImage(this.getDocumentBase(), "dolphin1.gif");
        this.m_mediaTracker.addImage(Dolphin.m_imageDolphin[1], 30);
        Dolphin.m_imageDolphin[2] = this.getImage(this.getDocumentBase(), "dolphin2.gif");
        this.m_mediaTracker.addImage(Dolphin.m_imageDolphin[2], 31);
        Dolphin.m_imageDolphin[3] = this.getImage(this.getDocumentBase(), "dolphin3.gif");
        this.m_mediaTracker.addImage(Dolphin.m_imageDolphin[3], 32);
        Package.m_imagePackage = this.getImage(this.getDocumentBase(), "package.gif");
        this.m_mediaTracker.addImage(Package.m_imagePackage, 33);
        VerticalTorpedo.m_imageBlast[0] = this.getImage(this.getDocumentBase(), "vtblast0.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageBlast[0], 34);
        VerticalTorpedo.m_imageBlast[1] = this.getImage(this.getDocumentBase(), "vtblast1.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageBlast[1], 35);
        VerticalTorpedo.m_imageBlast[2] = this.getImage(this.getDocumentBase(), "vtblast2.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageBlast[2], 36);
        VerticalTorpedo.m_imageBlast[3] = this.getImage(this.getDocumentBase(), "vtblast3.gif");
        this.m_mediaTracker.addImage(VerticalTorpedo.m_imageBlast[3], 37);
        KillerFish.m_imageFish = this.getImage(this.getDocumentBase(), "klr_fsh0.gif");
        this.m_mediaTracker.addImage(KillerFish.m_imageFish, 38);
        this.m_imageWater = this.getImage(this.getDocumentBase(), "water.gif");
        this.m_mediaTracker.addImage(this.m_imageWater, 39);
        Destroyer.m_imageShip = this.getImage(this.getDocumentBase(), "destroyr.gif");
        this.m_mediaTracker.addImage(Destroyer.m_imageShip, 40);
        Destroyer.m_imageWake[0] = this.getImage(this.getDocumentBase(), "rt_wake0.gif");
        this.m_mediaTracker.addImage(Destroyer.m_imageWake[0], 41);
        Destroyer.m_imageWake[1] = this.getImage(this.getDocumentBase(), "rt_wake1.gif");
        this.m_mediaTracker.addImage(Destroyer.m_imageWake[1], 42);
        Destroyer.m_imageWake[2] = this.getImage(this.getDocumentBase(), "rt_wake2.gif");
        this.m_mediaTracker.addImage(Destroyer.m_imageWake[2], 43);
        Destroyer.m_imageWake[3] = this.getImage(this.getDocumentBase(), "rt_wake3.gif");
        this.m_mediaTracker.addImage(Destroyer.m_imageWake[3], 44);
        DepthCharge.imageBlast[0] = this.getImage(this.getDocumentBase(), "dcblast0.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageBlast[0], 45);
        DepthCharge.imageBlast[1] = this.getImage(this.getDocumentBase(), "dcblast1.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageBlast[1], 46);
        DepthCharge.imageBlast[2] = this.getImage(this.getDocumentBase(), "dcblast2.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageBlast[2], 47);
        DepthCharge.imageBlast[3] = this.getImage(this.getDocumentBase(), "dcblast3.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageBlast[3], 48);
        DepthCharge.imageSplash[0] = this.getImage(this.getDocumentBase(), "splash0.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageSplash[0], 49);
        DepthCharge.imageSplash[1] = this.getImage(this.getDocumentBase(), "splash1.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageSplash[1], 50);
        DepthCharge.imageSplash[2] = this.getImage(this.getDocumentBase(), "splash2.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageSplash[2], 51);
        DepthCharge.imageSplash[3] = this.getImage(this.getDocumentBase(), "splash3.gif");
        this.m_mediaTracker.addImage(DepthCharge.imageSplash[3], 52);
        EnemySub.m_imageSub = this.getImage(this.getDocumentBase(), "enemysub.gif");
        this.m_mediaTracker.addImage(EnemySub.m_imageSub, 53);
        EnemyTorpedo.m_imageTorpedo = this.getImage(this.getDocumentBase(), "enemytor.gif");
        this.m_mediaTracker.addImage(EnemyTorpedo.m_imageTorpedo, 54);
        EnemyTorpedo.m_imageWake[0] = this.getImage(this.getDocumentBase(), "et_wake0.gif");
        this.m_mediaTracker.addImage(EnemyTorpedo.m_imageWake[0], 55);
        EnemyTorpedo.m_imageWake[1] = this.getImage(this.getDocumentBase(), "et_wake1.gif");
        this.m_mediaTracker.addImage(EnemyTorpedo.m_imageWake[1], 56);
        EnemyTorpedo.m_imageWake[2] = this.getImage(this.getDocumentBase(), "et_wake2.gif");
        this.m_mediaTracker.addImage(EnemyTorpedo.m_imageWake[2], 57);
        EnemyTorpedo.m_imageWake[3] = this.getImage(this.getDocumentBase(), "et_wake3.gif");
        this.m_mediaTracker.addImage(EnemyTorpedo.m_imageWake[3], 58);
        HorizontalTorpedo.m_imageTorpedo = this.getImage(this.getDocumentBase(), "htorpedo.gif");
        this.m_mediaTracker.addImage(HorizontalTorpedo.m_imageTorpedo, 59);
        MagneticMine.m_imageMine = this.getImage(this.getDocumentBase(), "mag_mine.gif");
        this.m_mediaTracker.addImage(MagneticMine.m_imageMine, 60);
        Dimension dimension = this.size();
        m_nWindowWidth = dimension.width;
        m_nWindowHeight = dimension.height;
        this.m_imageLoading = this.createImage(dimension.width, dimension.height);
        Graphics graphics = this.m_imageLoading.getGraphics();
        int n2 = 200;
        int n3 = 30;
        int n4 = 2;
        int n5 = (dimension.width - n2) / 2;
        int n6 = dimension.height * 2 / 3 - n3 / 2;
        int n7 = n5 + n4;
        int n8 = n6 + n4;
        int n9 = n2 - 2 * n4;
        int n10 = n3 - 2 * n4;
        int n11 = 61;
        int n12 = -1;
        graphics.setColor(Color.white);
        graphics.fillRect(0, 0, dimension.width, dimension.height);
        graphics.setColor(Color.black);
        graphics.setFont(new Font("Helvetica", 1, 24));
        FontMetrics fontMetrics = graphics.getFontMetrics();
        String string = "Torpedo Alley";
        int n13 = (dimension.width - fontMetrics.stringWidth(string)) / 2;
        int n14 = dimension.height / 3;
        graphics.drawString(string, n13, n14);
        n14 += fontMetrics.getDescent();
        graphics.setFont(new Font("Helvetica", 0, 14));
        fontMetrics = graphics.getFontMetrics();
        String string2 = "Copyright \u00a9 1982, 1997 Edward R. Hobbs";
        n13 = (dimension.width - fontMetrics.stringWidth(string2)) / 2;
        graphics.drawString(string2, n13, n14 += fontMetrics.getAscent() + fontMetrics.getLeading());
        int n15 = n14 + fontMetrics.getDescent();
        graphics.setColor(Color.black);
        for (int i = 0; i < n4; ++i) {
            graphics.drawRect(n5 + i, n6 + i, n2 - 2 * i - 1, n3 - 2 * i - 1);
        }
        String string3 = "Loading graphics...";
        n13 = (dimension.width - fontMetrics.stringWidth(string3)) / 2;
        n14 = n6 - fontMetrics.getDescent() - 2;
        graphics.drawString(string3, n13, n14);
        graphics.dispose();
        while (true) {
            int n16 = 0;
            for (n = 0; n < n11; ++n) {
                if (!this.m_mediaTracker.checkID(n, true)) continue;
                ++n16;
            }
            if (n16 > n12) {
                n12 = n16;
                this.DrawProgressBar(n7, n8, n9, n10, (double)n12 / (double)n11);
            }
            if (n12 >= n11) break;
            try {
                Thread.sleep(100L);
            }
            catch (InterruptedException interruptedException) {}
        }
        graphics = this.m_imageLoading.getGraphics();
        graphics.setFont(new Font("Helvetica", 0, 14));
        fontMetrics = graphics.getFontMetrics();
        graphics.setColor(Color.white);
        graphics.fillRect(0, n15, dimension.width, dimension.height - n15);
        graphics.setColor(Color.black);
        for (n = 0; n < n4; ++n) {
            graphics.drawRect(n5 + n, n6 + n, n2 - 2 * n - 1, n3 - 2 * n - 1);
        }
        string3 = "Loading sounds...";
        n13 = (dimension.width - fontMetrics.stringWidth(string3)) / 2;
        n14 = n6 - fontMetrics.getDescent() - 2;
        graphics.drawString(string3, n13, n14);
        graphics.dispose();
        this.DrawProgressBar(n7, n8, n9, n10, 0.0);
        this.m_acBigExplosion = this.getAudioClip(this.getDocumentBase(), "big_expl.au");
        this.m_acBigExplosion.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.12);
        playBigExplosion = false;
        this.m_acLittleExplosion = this.getAudioClip(this.getDocumentBase(), "ltl_expl.au");
        this.m_acLittleExplosion.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.25);
        m_bPlayLittleExplosion = false;
        this.m_acFireTorpedo = this.getAudioClip(this.getDocumentBase(), "fire.au");
        this.m_acFireTorpedo.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.37);
        m_bPlayFireTorpedo = false;
        this.m_acOutOfTorpedoes = this.getAudioClip(this.getDocumentBase(), "empty.au");
        this.m_acOutOfTorpedoes.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.5);
        m_bPlayOutOfTorpedoes = false;
        this.m_acClank = this.getAudioClip(this.getDocumentBase(), "clank.au");
        this.m_acClank.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.62);
        m_bPlayClank = false;
        this.m_acSupplies = this.getAudioClip(this.getDocumentBase(), "supplies.au");
        this.m_acSupplies.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.75);
        m_bPlaySupplies = false;
        this.m_acSplash = this.getAudioClip(this.getDocumentBase(), "splash.au");
        this.m_acSplash.play();
        this.DrawProgressBar(n7, n8, n9, n10, 0.87);
        m_bPlaySplash = false;
        this.m_acShark = this.getAudioClip(this.getDocumentBase(), "shark.au");
        this.m_acShark.play();
        this.DrawProgressBar(n7, n8, n9, n10, 1.0);
        m_bPlayShark = false;
        this.m_imageLoading.flush();
        playfieldWidth = m_nWindowWidth;
        m_nPlayfieldHeight = m_nWindowHeight - 20;
        this.m_bufferPlayfield = this.createImage(playfieldWidth, m_nPlayfieldHeight);
        this.m_bufferSurface = this.createImage(playfieldWidth, 60);
        Graphics graphics2 = this.m_bufferSurface.getGraphics();
        int n17 = this.m_imageWater.getWidth(null);
        int n18 = this.m_imageWater.getHeight(null);
        for (int i = 0; i < playfieldWidth; i += n17) {
            int n19 = 0;
            do {
                graphics2.drawImage(this.m_imageWater, i, n19, null);
            } while ((n19 += n18) < 60);
        }
        graphics2.dispose();
        mission = 1;
        score = 0;
        m_nSubsLeft = 3;
        this.m_bufferScorebar = this.createImage(playfieldWidth, 20);
        Font font = new Font("Helvetica", 1, 16);
        this.m_fontScorebar = new Font("Helvetica", 0, 16);
        Graphics graphics3 = this.m_bufferScorebar.getGraphics();
        graphics3.setColor(SCOREBAR_BACKGROUND_COLOR);
        graphics3.fillRect(0, 0, playfieldWidth, 20);
        graphics3.setColor(SCOREBAR_CAPTION_COLOR);
        int n20 = 8;
        int n21 = 0;
        do {
            String string4 = null;
            String string5 = null;
            switch (n21) {
                case 0: {
                    string4 = new String("Mission:");
                    string5 = new String("0");
                    break;
                }
                case 1: {
                    string4 = new String("Ships sunk:");
                    string5 = new String("10");
                    break;
                }
                case 2: {
                    string4 = new String("Score:");
                    string5 = new String("000000");
                    break;
                }
                case 3: {
                    string4 = new String("Torpedoes:");
                    string5 = new String("00");
                    break;
                }
                case 4: {
                    string4 = new String("Fuel:");
                    string5 = new String("100%");
                }
            }
            graphics3.setFont(font);
            object = graphics3.getFontMetrics();
            this.m_nScorebarZoneCaptionWidth[n21] = ((FontMetrics)object).stringWidth(string4);
            n20 += this.m_nScorebarZoneCaptionWidth[n21];
            graphics3.setFont(this.m_fontScorebar);
            FontMetrics fontMetrics2 = graphics3.getFontMetrics();
            this.m_nScorebarZoneDataWidth[n21] = fontMetrics2.stringWidth(string5);
            n20 += this.m_nScorebarZoneDataWidth[n21];
            n20 += 4;
        } while (++n21 < 5);
        n21 = (playfieldWidth - n20) / 4;
        int n22 = 4;
        graphics3.setFont(font);
        graphics3.getFontMetrics();
        int n23 = 0;
        do {
            object = null;
            switch (n23) {
                case 0: {
                    object = new String("Mission:");
                    break;
                }
                case 1: {
                    object = new String("Ships sunk:");
                    break;
                }
                case 2: {
                    object = new String("Score:");
                    break;
                }
                case 3: {
                    object = new String("Torpedoes:");
                    break;
                }
                case 4: {
                    object = new String("Fuel:");
                }
            }
            graphics3.drawString((String)object, n22, 16);
            this.m_nScorebarZoneDataX[n23] = n22 + this.m_nScorebarZoneCaptionWidth[n23] + 4;
            this.m_nScorebarZoneValue[n23] = -1;
            n22 = this.m_nScorebarZoneDataX[n23] + this.m_nScorebarZoneDataWidth[n23] + n21;
        } while (++n23 < 5);
        graphics3.dispose();
        Graphics graphics4 = this.m_bufferPlayfield.getGraphics();
        graphics4.setFont(this.m_fontMessage);
        object = graphics4.getFontMetrics();
        int n24 = ((FontMetrics)object).stringWidth(this.m_stringStart);
        this.m_nXstartString = (playfieldWidth - n24) / 2;
        this.m_nYstartString = 60 + (m_nPlayfieldHeight - 60) / 2;
        n24 = ((FontMetrics)object).stringWidth(this.m_stringOutOfFuel);
        this.m_nXoutOfFuel = (playfieldWidth - n24) / 2;
        this.m_nYoutOfFuel = 60 + (m_nPlayfieldHeight - 60) * 3 / 4;
        n24 = ((FontMetrics)object).stringWidth(this.m_stringGameOver);
        this.m_nXgameOver = (playfieldWidth - n24) / 2;
        this.m_nYgameOver = 60 + (m_nPlayfieldHeight - 60) / 4;
        n24 = ((FontMetrics)object).stringWidth(this.m_stringMissionTitle);
        this.m_nXmissionTitle = (playfieldWidth - n24) / 2;
        this.m_nYmissionTitle = 60 + (m_nPlayfieldHeight - 60) / 4;
        n24 = ((FontMetrics)object).stringWidth(this.m_stringMissionComplete);
        this.m_nXmissionComplete = (playfieldWidth - n24) / 2;
        this.m_nYmissionComplete = 60 + (m_nPlayfieldHeight - 60) / 2;
        this.m_stringSubsLeft[2] = new String("3 subs left");
        n24 = ((FontMetrics)object).stringWidth(this.m_stringSubsLeft[2]);
        this.m_nXsubsLeft[2] = (playfieldWidth - n24) / 2;
        this.m_stringSubsLeft[1] = new String("2 subs left");
        n24 = ((FontMetrics)object).stringWidth(this.m_stringSubsLeft[1]);
        this.m_nXsubsLeft[1] = (playfieldWidth - n24) / 2;
        this.m_stringSubsLeft[0] = new String("Last sub!!");
        n24 = ((FontMetrics)object).stringWidth(this.m_stringSubsLeft[0]);
        this.m_nXsubsLeft[0] = (playfieldWidth - n24) / 2;
        this.m_nYsubsLeft = 60 + (m_nPlayfieldHeight - 60) * 4 / 5;
        graphics4.dispose();
        player.OneTimeInitialization();
        CapitalShip.xSpacing = m_nWindowWidth / 2;
        CapitalShip.wakeWidth = CapitalShip.imageWake[0].getWidth(null);
        CapitalShip.wakeHeight = CapitalShip.imageWake[0].getHeight(null);
        int n25 = 0;
        do {
            Seafox.m_capitalShip[n25] = new CapitalShip();
        } while (++n25 < 10);
        HospitalShip.m_nXspacing = m_nWindowWidth / 2;
        n25 = 0;
        do {
            Seafox.m_hospitalShip[n25] = new HospitalShip();
        } while (++n25 < 3);
        Destroyer.m_dShipWidth = Destroyer.m_imageShip.getWidth(null);
        Destroyer.m_nShipHeight = Destroyer.m_imageShip.getHeight(null);
        Destroyer.m_nWakeWidth = Destroyer.m_imageWake[0].getWidth(null);
        Destroyer.m_nWakeHeight = Destroyer.m_imageWake[0].getHeight(null);
        n25 = 0;
        do {
            Seafox.m_destroyer[n25] = new Destroyer();
        } while (++n25 < 3);
        n25 = 0;
        do {
            Seafox.m_depthCharge[n25] = new DepthCharge();
        } while (++n25 < 3);
        EnemySub.m_dSubWidth = EnemySub.m_imageSub.getWidth(null);
        EnemySub.m_dSubHeight = EnemySub.m_imageSub.getHeight(null);
        EnemySub.m_dYlaunchMin = Player.m_dYmin;
        EnemySub.m_dYlaunchMax = (double)m_nPlayfieldHeight - 2.0 * EnemySub.m_dSubHeight;
        n25 = 0;
        do {
            Seafox.m_enemySub[n25] = new EnemySub();
        } while (++n25 < 3);
        VerticalTorpedo.m_nTorpedoWidth = VerticalTorpedo.m_imageUp.getWidth(null);
        VerticalTorpedo.m_nTorpedoHeight = VerticalTorpedo.m_imageUp.getHeight(null);
        VerticalTorpedo.m_nBlastWidth = VerticalTorpedo.m_imageBlast[0].getWidth(null);
        VerticalTorpedo.m_nBlastHeight = VerticalTorpedo.m_imageBlast[0].getHeight(null);
        HorizontalTorpedo.m_dTorpedoWidth = HorizontalTorpedo.m_imageTorpedo.getWidth(null);
        HorizontalTorpedo.m_dTorpedoHeight = HorizontalTorpedo.m_imageTorpedo.getHeight(null);
        n25 = 0;
        do {
            Seafox.m_enemyTorpedo[n25] = new EnemyTorpedo();
        } while (++n25 < 3);
        EnemyTorpedo.m_dTorpedoWidth = EnemyTorpedo.m_imageTorpedo.getWidth(null);
        EnemyTorpedo.m_dTorpedoHeight = EnemyTorpedo.m_imageTorpedo.getHeight(null);
        EnemyTorpedo.m_dWakeWidth = EnemyTorpedo.m_imageWake[0].getWidth(null);
        n25 = 0;
        do {
            Seafox.m_magneticMine[n25] = new MagneticMine();
            Seafox.m_magneticMine[n25].m_ndx = n25;
        } while (++n25 < 3);
        MagneticMine.m_dMineRadius = MagneticMine.m_imageMine.getWidth(null) / 2;
        SupplySub.m_dImageWidth = SupplySub.m_imageSub.getWidth(null);
        SupplySub.m_dImageHeight = SupplySub.m_imageSub.getHeight(null);
        SupplySub.m_dY = (double)m_nPlayfieldHeight - SupplySub.m_dImageHeight;
        SupplySub.m_dXrelease = (double) playfieldWidth / 4.0;
        Dolphin.m_dImageWidth = Dolphin.m_imageDolphin[0].getWidth(null);
        Package.m_dPackageWidth = Package.m_imagePackage.getWidth(null);
        Package.m_dPackageHeight = Package.m_imagePackage.getHeight(null);
        this.MissionInit();
        this.SubInit();
        m_bGameOver = true;
        Player.m_nStatus = 2;
        Seafox.player.m_lRestartTime = 1L;
        m_nSubsLeft = 0;
        m_bDisplayMissionTitle = false;
        Destroyer.m_nShipLimit = 3;
        DepthCharge.chargeLimit = 3;
        EnemySub.m_nSubLimit = 3;
        EnemyTorpedo.m_nTorpedoLimit = 3;
        MagneticMine.m_nMineLimit = 3;
        this.m_bInitialized = true;
    }

    private void SubInit() {
        player.MissionInitialization();
        CapitalShip.startTime = System.currentTimeMillis();
        CapitalShip.xOriginStart = -(CapitalShip.SHIPS * CapitalShip.xSpacing);
        HospitalShip.m_lStartTime = System.currentTimeMillis();
        HospitalShip.m_dXoriginStart = -(3 * HospitalShip.m_nXspacing);
        int n = 0;
        do {
            Seafox.m_destroyer[n].m_nStatus = 4;
        } while (++n < 3);
        Destroyer.m_lTimeNextLaunch = System.currentTimeMillis() + 5000L;
        Destroyer.m_nActiveShips = 0;
        n = 0;
        do {
            Seafox.m_depthCharge[n].status = 0;
        } while (++n < 3);
        DepthCharge.activeCharges = 0;
        n = 0;
        do {
            Seafox.m_enemySub[n].m_nStatus = 4;
        } while (++n < 3);
        EnemySub.m_lTimeNextLaunch = System.currentTimeMillis() + 7000L;
        EnemySub.m_nActiveSubs = 0;
        n = 0;
        do {
            Seafox.m_enemyTorpedo[n].m_nStatus = 0;
        } while (++n < 3);
        EnemyTorpedo.m_nActiveTorpedoes = 0;
        n = 0;
        do {
            Seafox.m_magneticMine[n].m_nStatus = 0;
        } while (++n < 3);
        MagneticMine.m_nActiveMines = 0;
        Seafox.m_verticalTorpedo.m_nStatus = 0;
        Seafox.m_horizontalTorpedo.m_nStatus = 0;
        Seafox.m_supplySub.m_lLaunchTime = System.currentTimeMillis();
        Seafox.m_supplySub.m_nStatus = 0;
        Seafox.m_dolphin.m_bActive = false;
        Seafox.m_package.m_nStatus = 2;
        Seafox.m_killerFish.m_bActive = false;
    }

    private void PlaySoundEffects() {
        if (playBigExplosion) {
            this.m_acBigExplosion.play();
            playBigExplosion = false;
        }
        if (m_bPlayLittleExplosion) {
            this.m_acLittleExplosion.play();
            m_bPlayLittleExplosion = false;
        }
        if (m_bPlayFireTorpedo) {
            this.m_acFireTorpedo.play();
            m_bPlayFireTorpedo = false;
        }
        if (m_bPlayOutOfTorpedoes) {
            this.m_acOutOfTorpedoes.play();
            m_bPlayOutOfTorpedoes = false;
        }
        if (m_bPlayClank) {
            this.m_acClank.play();
            m_bPlayClank = false;
        }
        if (m_bPlaySupplies) {
            this.m_acSupplies.play();
            m_bPlaySupplies = false;
        }
        if (m_bPlaySplash) {
            this.m_acSplash.play();
            m_bPlaySplash = false;
        }
        if (m_bPlayShark) {
            this.m_acShark.play();
            m_bPlayShark = false;
        }
    }
}
