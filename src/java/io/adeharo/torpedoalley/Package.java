package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 */
import java.awt.Graphics;
import java.awt.Image;

class Package {
    public static Image m_imagePackage;
    public static double m_dPackageWidth;
    public static double m_dPackageHeight;
    public int m_nStatus;
    public static final int ON_DOLPHIN = 0;
    static final int FALLING = 1;
    public static final int INACTIVE = 2;
    final double FALL_VELOCITY = 50.0;
    double m_dYstartFall;
    long m_lFallStartTime;
    double m_dX;
    double m_dY;

    public void Draw(Graphics graphics) {
        if (this.m_nStatus == 2) {
            return;
        }
        int n = (int)(this.m_dX + 0.5);
        int n2 = (int)(this.m_dY + 0.5);
        graphics.drawImage(m_imagePackage, n, n2, null);
    }

    Package() {
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (this.m_nStatus == 2) {
            return false;
        }
        if (d >= this.m_dX && d <= this.m_dX + m_dPackageWidth && d2 >= this.m_dY && d2 <= this.m_dY + m_dPackageHeight) {
            this.m_nStatus = 2;
            return true;
        }
        return false;
    }

    public void StartFalling() {
        if (this.m_nStatus == 0) {
            this.m_nStatus = 1;
            this.m_dYstartFall = this.m_dY;
            this.m_lFallStartTime = Seafox.latestTime;
        }
    }

    public void Update() {
        if (this.m_nStatus == 2) {
            return;
        }
        if (this.m_nStatus == 1) {
            this.m_dY = this.m_dYstartFall + 50.0 * (double)(Seafox.latestTime - this.m_lFallStartTime) / 1000.0;
            if (this.m_dY >= (double)Seafox.m_nPlayfieldHeight) {
                this.m_nStatus = 2;
                return;
            }
        }
        if (this.m_nStatus == 0) {
            Dolphin dolphin = Seafox.m_dolphin;
            this.m_dX = dolphin.m_dX + 23.0;
            this.m_dY = dolphin.m_dY - m_dPackageHeight;
            switch (dolphin.m_ndxImage) {
                case 1: {
                    this.m_dY += 2.0;
                    break;
                }
                case 2: {
                    this.m_dY += 4.0;
                    break;
                }
                case 3: {
                    this.m_dY += 6.0;
                }
            }
            if (this.m_dX >= (double)Seafox.playfieldWidth) {
                this.m_nStatus = 2;
                return;
            }
        }
        if (Seafox.player.CollisionAnalysis(this)) {
            this.m_nStatus = 2;
            return;
        }
        int n = 0;
        do {
            if (!Seafox.m_depthCharge[n].CollisionAnalysis(this.m_dX, this.m_dY, this.m_dX + m_dPackageWidth, this.m_dY + m_dPackageHeight)) continue;
            this.m_nStatus = 2;
        } while (++n < 3);
        n = 0;
        do {
            if (!Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX, this.m_dY, this.m_dX + m_dPackageWidth, this.m_dY + m_dPackageHeight)) continue;
            this.m_nStatus = 2;
        } while (++n < 3);
    }
}
