package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  EnemyTorpedo
 *  MagneticMine
 *  Player
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

class EnemySub {
    public static Image m_imageSub;
    public static double m_dSubWidth;
    public static double m_dSubHeight;
    public static final int MAX_SUBS = 3;
    public static int m_nSubLimit;
    public static int m_nActiveSubs;
    static final int MIN_LAUNCH_INTERVAL = 10000;
    static final int MAX_LAUNCH_INTERVAL = 15000;
    public static long m_lTimeNextLaunch;
    public static double m_dYlaunchMin;
    public static double m_dYlaunchMax;
    public static final double MIN_VELOCITY = 15.0;
    public static final double MAX_VELOCITY = 25.0;
    public static final double Y_VELOCITY = 5.0;
    public static final int POINTS_FOR_SINKING = 500;
    static final double LEFT_TORPEDO_MARGIN = 0.0;
    static final double RIGHT_TORPEDO_MARGIN = 100.0;
    static final double LEFT_MINE_MARGIN = 100.0;
    static final double RIGHT_MINE_MARGIN = 40.0;
    public static final double CONNING_TOWER_LEFT = 19.0;
    public static final double CONNING_TOWER_RIGHT = 31.0;
    public static final double CONNING_TOWER_TOP = 0.0;
    public static final double CONNING_TOWER_BOTTOM = 8.0;
    public static final double HULL_LEFT = 1.0;
    public static final double HULL_RIGHT = 57.0;
    public static final double HULL_TOP = 8.0;
    public static final double HULL_BOTTOM = 20.0;
    public int m_nStatus;
    static final int NORMAL = 0;
    static final int EXPLODING = 1;
    static final int SCORING = 3;
    public static final int INACTIVE = 4;
    long m_lTimeStatusUpdated;
    long m_lTimeLastUpdated;
    long m_lTimeTorpedo;
    long m_lTimeMine;
    int m_nPoints;
    double m_dXlaunch;
    public double m_dX;
    public double m_dY;
    double m_dVelocity;

    public void Draw(Graphics graphics) {
        int n = 0;
        int n2 = 0;
        Image image = null;
        if (this.m_nStatus == 4) {
            return;
        }
        if (this.m_nStatus == 0 || this.m_nStatus == 1) {
            n = (int)(this.m_dX + 0.5);
            n2 = (int)this.m_dY;
            graphics.drawImage(m_imageSub, n, n2, null);
            if (this.m_nStatus == 1) {
                image = CapitalShip.imageBlast[(Seafox.random.nextInt() & Integer.MAX_VALUE) % 4];
                graphics.drawImage(image, n += ((int)m_dSubWidth - image.getWidth(null)) / 2, n2 += ((int)m_dSubHeight - image.getHeight(null)) / 2, null);
            }
            return;
        }
        if (this.m_nStatus == 3) {
            graphics.setFont(Seafox.fontScorePopup);
            graphics.setColor(Color.yellow);
            String string = new String("" + this.m_nPoints);
            n = (int)this.m_dX + 10;
            n2 = (int)(this.m_dY + m_dSubHeight);
            graphics.drawString(string, n, n2);
        }
    }

    public void Launch() {
        this.m_nStatus = 0;
        this.m_lTimeStatusUpdated = Seafox.latestTime;
        this.m_lTimeLastUpdated = Seafox.latestTime;
        ++m_nActiveSubs;
        this.m_dX = this.m_dXlaunch = -m_dSubWidth;
        this.m_dY = m_dYlaunchMin + Seafox.random.nextDouble() * (m_dYlaunchMax - m_dYlaunchMin);
        this.m_dVelocity = 15.0 + 10.0 * Seafox.random.nextDouble();
        this.m_nPoints = 0;
        m_lTimeNextLaunch = this.m_lTimeStatusUpdated + 10000L + (long)(5000.0 * Seafox.random.nextDouble());
        this.m_lTimeTorpedo = Seafox.latestTime + (long)(10000.0 * Seafox.random.nextDouble());
        this.m_lTimeMine = Seafox.latestTime + (long)(10000.0 * Seafox.random.nextDouble());
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (this.m_nStatus == 0 && d >= this.m_dX && d <= this.m_dX + m_dSubWidth && d2 >= this.m_dY && d2 <= this.m_dY + m_dSubHeight) {
            double d3 = d - this.m_dX;
            double d4 = d2 - this.m_dY;
            if (d3 >= 19.0 && d3 <= 31.0 && d4 >= 0.0 && d4 <= 8.0 || d3 >= 1.0 && d3 <= 57.0 && d4 >= 8.0 && d4 <= 20.0) {
                this.m_nStatus = 1;
                this.m_nPoints = 500;
                Seafox.score += this.m_nPoints;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return true;
            }
        }
        return false;
    }

    EnemySub() {
    }

    public void Update() {
        if (this.m_nStatus == 4) {
            if (m_nActiveSubs < m_nSubLimit && Seafox.latestTime >= m_lTimeNextLaunch) {
                this.Launch();
            }
            return;
        }
        if (this.m_nStatus == 0) {
            this.m_dX = this.m_dXlaunch + this.m_dVelocity * (double)(Seafox.latestTime - this.m_lTimeStatusUpdated) / 1000.0;
            if (this.m_dX > (double)Seafox.playfieldWidth) {
                this.m_nStatus = 4;
                m_nActiveSubs += -1;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                return;
            }
            if (Player.m_nStatus == 0) {
                if (Player.m_dY < this.m_dY) {
                    this.m_dY -= 5.0 * (double)(Seafox.latestTime - this.m_lTimeLastUpdated) / 1000.0;
                    if (this.m_dY < Player.m_dY) {
                        this.m_dY = Player.m_dY;
                    }
                }
                if (Player.m_dY > this.m_dY) {
                    this.m_dY += 5.0 * (double)(Seafox.latestTime - this.m_lTimeLastUpdated) / 1000.0;
                    if (this.m_dY > Player.m_dY) {
                        this.m_dY = Player.m_dY;
                    }
                }
            }
            if (Seafox.player.CollisionAnalysis(this)) {
                this.m_nStatus = 1;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return;
            }
            int n = 0;
            do {
                if (!Seafox.m_depthCharge[n].CollisionAnalysis(this.m_dX + 19.0, this.m_dY + 0.0, this.m_dX + 31.0, this.m_dY + 8.0) && !Seafox.m_depthCharge[n].CollisionAnalysis(this.m_dX + 1.0, this.m_dY + 8.0, this.m_dX + 57.0, this.m_dY + 20.0)) continue;
                this.m_nStatus = 1;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return;
            } while (++n < 3);
            n = 0;
            do {
                if (!Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX + 19.0, this.m_dY + 0.0, this.m_dX + 31.0, this.m_dY + 8.0) && !Seafox.m_magneticMine[n].CollisionAnalysis(this.m_dX + 1.0, this.m_dY + 8.0, this.m_dX + 57.0, this.m_dY + 20.0)) continue;
                this.m_nStatus = 1;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                Seafox.playBigExplosion = true;
                return;
            } while (++n < 3);
            if (this.m_dX >= 0.0 && this.m_dX <= (double)Seafox.playfieldWidth - m_dSubWidth - 100.0 && Seafox.latestTime >= this.m_lTimeTorpedo && EnemyTorpedo.m_nActiveTorpedoes < EnemyTorpedo.m_nTorpedoLimit) {
                n = 0;
                n = 0;
                while (Seafox.m_enemyTorpedo[n].m_nStatus != 0 && ++n < 3) {
                }
                if (n < 3) {
                    this.m_lTimeTorpedo = Seafox.latestTime + 12000L + (long)(Seafox.random.nextDouble() * 6000.0);
                    Seafox.m_enemyTorpedo[n].Fire(this);
                }
            }
            if (this.m_dX >= 100.0 && this.m_dX <= (double)Seafox.playfieldWidth - m_dSubWidth - 40.0 && Seafox.latestTime >= this.m_lTimeMine && MagneticMine.m_nActiveMines < MagneticMine.m_nMineLimit) {
                n = 0;
                n = 0;
                while (Seafox.m_magneticMine[n].m_nStatus != 0 && ++n < 3) {
                }
                if (n < 3) {
                    this.m_lTimeMine = Seafox.latestTime + 10000L + (long)(Seafox.random.nextDouble() * 5000.0);
                    Seafox.m_magneticMine[n].Launch(this);
                }
            }
            this.m_lTimeLastUpdated = Seafox.latestTime;
            return;
        }
        if (this.m_nStatus == 1) {
            if ((int)(Seafox.latestTime - this.m_lTimeStatusUpdated) >= 500) {
                if (this.m_nPoints > 0) {
                    this.m_nStatus = 3;
                } else {
                    this.m_nStatus = 4;
                    m_nActiveSubs += -1;
                }
                this.m_lTimeStatusUpdated = Seafox.latestTime;
                return;
            }
            Seafox.player.CollisionAnalysis(this);
            return;
        }
        if (this.m_nStatus == 3) {
            if ((int)(Seafox.latestTime - this.m_lTimeStatusUpdated) >= 999) {
                this.m_nStatus = 4;
                m_nActiveSubs += -1;
                this.m_lTimeStatusUpdated = Seafox.latestTime;
            }
            return;
        }
    }
}
