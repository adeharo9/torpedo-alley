package io.adeharo.torpedoalley;

/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  SupplySub
 */
import java.awt.Graphics;
import java.awt.Image;

class Dolphin {
    public static final int IMAGES = 4;
    public static Image[] m_imageDolphin = new Image[4];
    public static double m_dImageWidth;
    static final double X_VELOCITY = 20.0;
    static final double Y_VELOCITY = -8.0;
    static final int FREQUENCY = 100;
    double m_dXrelease;
    double m_dYrelease;
    long m_lReleaseTime;
    public boolean m_bActive;
    public double m_dX;
    public double m_dY;
    int m_ndxImage;

    public void Draw(Graphics graphics) {
        if (!this.m_bActive) {
            return;
        }
        int n = (int)(this.m_dX + 0.5);
        int n2 = (int)(this.m_dY + 0.5);
        graphics.drawImage(m_imageDolphin[this.m_ndxImage], n, n2, null);
    }

    public void Release(SupplySub supplySub) {
        this.m_dXrelease = supplySub.m_dX + 10.0;
        this.m_dYrelease = SupplySub.m_dY;
        this.m_lReleaseTime = Seafox.latestTime;
        this.m_bActive = true;
        this.m_dX = this.m_dXrelease;
        this.m_dY = this.m_dYrelease;
        this.m_ndxImage = 0;
        Seafox.m_package.m_nStatus = 0;
    }

    public boolean CollisionAnalysis(double d, double d2) {
        if (!this.m_bActive) {
            return false;
        }
        double d3 = 0.0;
        switch (this.m_ndxImage) {
            case 0: {
                d3 = 0.0;
                break;
            }
            case 1: {
                d3 = 2.0;
                break;
            }
            case 2: {
                d3 = 4.0;
                break;
            }
            case 3: {
                d3 = 6.0;
            }
        }
        if (d >= this.m_dX && d <= this.m_dX + m_dImageWidth && d2 >= this.m_dY + d3 && d2 <= this.m_dY + (double)m_imageDolphin[this.m_ndxImage].getHeight(null)) {
            this.m_bActive = false;
            Seafox.m_package.StartFalling();
            return true;
        }
        return false;
    }

    Dolphin() {
    }

    public void Update() {
        if (!this.m_bActive) {
            return;
        }
        int n = (int)(Seafox.latestTime - this.m_lReleaseTime);
        this.m_dX = this.m_dXrelease + 20.0 * (double)n / 1000.0;
        this.m_dY = this.m_dYrelease + -8.0 * (double)n / 1000.0;
        if (this.m_dX >= (double)Seafox.playfieldWidth) {
            this.m_bActive = false;
            return;
        }
        int n2 = n / 100 % 6;
        switch (n2) {
            case 0: {
                this.m_ndxImage = 0;
                break;
            }
            case 1: {
                this.m_ndxImage = 1;
                break;
            }
            case 2: {
                this.m_ndxImage = 2;
                break;
            }
            case 3: {
                this.m_ndxImage = 3;
                break;
            }
            case 4: {
                this.m_ndxImage = 2;
                break;
            }
            case 5: {
                this.m_ndxImage = 1;
            }
        }
        double d = this.m_dY;
        double d2 = this.m_dY + (double)m_imageDolphin[this.m_ndxImage].getWidth(null);
        switch (this.m_ndxImage) {
            case 1: {
                d += 2.0;
                break;
            }
            case 2: {
                d += 4.0;
                break;
            }
            case 3: {
                d += 6.0;
            }
        }
        int n3 = 0;
        do {
            if (!Seafox.m_depthCharge[n3].CollisionAnalysis(this.m_dX, d, this.m_dX + m_dImageWidth, d2)) continue;
            this.m_bActive = false;
            Seafox.m_package.StartFalling();
        } while (++n3 < 3);
        n3 = 0;
        do {
            if (!Seafox.m_magneticMine[n3].CollisionAnalysis(this.m_dX, d, this.m_dX + m_dImageWidth, d2)) continue;
            this.m_bActive = false;
            Seafox.m_package.StartFalling();
        } while (++n3 < 3);
    }
}
